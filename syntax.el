;; Major mode for the Core language

(setq ctor '(and (any "A-Z") (0+ (or alnum "_"))))
(setq ident `(and (or alpha "_") (0+ (or alnum "_"))))
(setq s_ident `(and "~" (1+ num) (0+ (and "." (eval ident)))))
(setq p_ident `(and "'" (eval ident) (0+ (and "." (eval ident)))))

(setq n_ang `(* (not (in "<" ">"))))
(setq n_bra `(* (not (in "[" "]"))))
(setq n_par `(* (not (in "(" ")"))))
(setq n_cur `(* (not (in "{" "}"))))
(setq b_typ `(* (not (in ":" "=" ";" ")" "["))))
(setq d_typ `(* (not (in ":" "=" ";" ")" "[" "-"))))

(setq n_par `(* (not (in "(" ")"))))
(setq n_arr `(* (not (in "(" ")" "-"))))

(setq poly-annot '(:inherit font-lock-type-face :slant italic))

(setq qvar `(and (? "'") ,ident))

(setq s_typ `(or ,n_arr (and "[" ,n_bra "]") (and "<" ,n_ang ">")))

(defun p- (i o) `(and ,o (* (and "(" ,i ")")) ,o))
(defun p-m (i o) (p- (p- (p- i i) i) o))

(setq mylsl-font-lock-keywords
      (let* (
             ;; define several category of keywords
             (x-struct '("let" "fix" "in"))
             (x-keywords '("fun" "if" "then" "else" "type" "size"))
             (x-types '("int" "bool" "const" "static" "dynamic"))
             (x-constants '("true" "false"))
             (x-functions '()) ;"if" "then" "else" "every" "last"))

             ;; generate regex string for each category of keywords
             (x-struct-regexp (regexp-opt x-struct 'words))
             (x-keywords-regexp (regexp-opt x-keywords 'words))
             (x-types-regexp (regexp-opt x-types 'words))
             (x-constants-regexp (regexp-opt x-constants 'words))
             )

        `(
          ; Extistential size (let size)
          (,(rx (group-n 1 (: "let size")) (+ (any space))
                (group-n 2 (eval ident)) (* (any space))
                "=")
           (1 '(:weight bold))
           (2 font-lock-constant-face))

          ; Pattern binders (let/fix)
          (,(rx (group-n 1 (or "let" "fix")) (+ (any space))
                (group-n 2 (eval ident)) (* (any space))
                (group-n 3 (? (and "[" (*? anything) (* (and "[" (*? anything) "]" (*? anything))) "]")))
                (* (any space))
                ":"
                (group-n 4 (*? anything)) "=")
           (1 '(:weight bold))
           (2 font-lock-function-name-face)
           (3 poly-annot)
           (4 font-lock-type-face))


          ; Type binder (fun)
          (,(rx (group-n 1 "fun") (+ (any space))
                (group-n 2 (eval ident)) (* (any space)) ":"
                (group-n 3 (eval (p-m n_par s_typ))) "->")
           (1 font-lock-keyword-face)
           (2 font-lock-variable-name-face)
           (3 font-lock-type-face))

          ; Generic type annotation
          (,(rx "(" (*? anything) ":>" (group-n 1 (*? anything)) ")")
           (1 font-lock-type-face))
          (,(rx "[" (*? anything) ":>" (group-n 1 (*? anything)) "]")
           (1 font-lock-type-face))
          (,(rx ":" (group-n 1 (eval (p-m n_par n_par))) (in ")"))
           (1 font-lock-type-face))

          ; Size/Type abstraction/application
          (,(rx "[" (* (and "[" (*? anything) "]" (*? anything))) (*? anything) "]") . poly-annot)

          ; Interger constant
          (,(rx (not (in "a-z" "A-Z" "_" "0-9")) (group-n 1 (* (any digit))))
           (1 font-lock-constant-face))

          ; Sizes
          (,(rx  "<" (* (not (in ">"))) ">" ) . font-lock-constant-face)

          ; Builtins
          (,(rx "$" (eval ident)) . font-lock-builtin-face)

          ; Special identifiers
          (,(concat (rx symbol-start) x-types-regexp (rx symbol-end)) . font-lock-type-face)
          (,(concat (rx symbol-start) x-struct-regexp (rx symbol-end)) (0 '(:weight bold)))
          (,(concat (rx symbol-start) x-keywords-regexp (rx symbol-end)) . font-lock-keyword-face)
          (,(concat (rx symbol-start) x-constants-regexp (rx symbol-end)) . font-lock-constant-face)

          ; Test markers
          (,(rx "//" (* (any space)) (or "FAIL" "SUCC"))
           (0 '(:inherit font-lock-comment-face :weight bold) t))

          )
        )
      )




(defvar core-mode-syntax-table nil "Syntax table for `core-mode'.")

(setq core-mode-syntax-table
      (let ( (synTable (make-syntax-table)))
        ;; make char's syntax for C++ style comment “// …”
        (modify-syntax-entry ?/ ". 124b" synTable)
        (modify-syntax-entry ?\n "> b" synTable)

        ;; comment style “/* … */”
;;        (modify-syntax-entry ?\/ ". 14" synTable)
        (modify-syntax-entry ?* ". 23n" synTable)

        synTable))


;;;###autoload
(define-derived-mode core-mode fundamental-mode "Core Language mode"
  "Major mode for editing programmes in the core language"

  ;; code for syntax highlighting
  (setq font-lock-defaults '((mylsl-font-lock-keywords)))


  (set-syntax-table core-mode-syntax-table)
  )

;; add the mode to the `features' list
(provide 'core-mode)











(setq surf-mode-font-lock
      (let* (
             ;; define several category of keywords
             (x-struct '("let" "rec" "in" "rec"))
             (x-keywords '("fun" "if" "then" "else" "type" "size"))
             (x-types '("int" "bool" "const" "static" "dynamic"))
             (x-constants '("true" "false"))
             (x-functions '("map" "fold" "scan")) ;"if" "then" "else" "every" "last"))

             ;; generate regex string for each category of keywords
             (x-struct-regexp (regexp-opt x-struct 'words))
             (x-keywords-regexp (regexp-opt x-keywords 'words))
             (x-types-regexp (regexp-opt x-types 'words))
             (x-constants-regexp (regexp-opt x-constants 'words))
;             (x-functions-regexp (regexp-opt x-functions 'words))
             )

        `(

          ; Extistential size (let size)
          (,(rx (group-n 1 (: "let size")) (+ (any space))
                (group-n 2 (eval ident)) (* (any space))
                "=")
           (1 '(:weight bold))
           (2 font-lock-constant-face))


          (,(rx symbol-start
                (group-n 1 (| "fix" (: "let" (? (: (+ (any space) "rec"))))) (+ (any space)))
                (group-n 2 (eval ident)))
           (1 '(:weight bold))
           (2 font-lock-function-name-face)

;           (,(rx (group-n 1 (eval ident)))
;            (save-excursion (re-search-forward (rx (*? anything) "=")) (point))
;            (re-search-backward (rx (| "let" "rec" "fix") (any space)))
;            (1 font-lock-variable-name-face t))

           (,(rx (| (: (group-n 1 (: "<<" (*? anything) ">>"))
                       (group-n 2 "") (group-n 3 ""))
                    (: (group-n 2 (eval ident))
                       (group-n 1 "") (group-n 3 ""))
                    (: "(" (group-n 3 (: (|"size" "type") (*? anything))) ")"
                       (group-n 1 "") (group-n 2 ""))
                    (: "(" (group-n 2 (*? anything)) ":" (group-n 3 (*? anything)) ")"
                       (group-n 1 ""))
                    (: "[" (group-n 2 (*? anything)) "<" (group-n 3 (*? anything)) "]"
                       (group-n 1 ""))
                    (: ":" (group-n 3 (* anything)) "="
                       (group-n 1 "") (group-n 2 ""))
                 ))
            (save-excursion (re-search-forward (rx (*? anything) "=")) (point))
            nil
            (1 font-lock-constant-face t)
            (2 font-lock-variable-name-face)
            (3 font-lock-type-face t)
            )
           )


          ;; (,(rx symbol-start
          ;;       (group-n 1 (| "fix" (: "let" (? (: (+ (any space) "rec"))))) (+ (any space)))
          ;;       (group-n 2 (eval ident)))
          ;;  (1 '(:weight bold))
          ;;  (2 font-lock-function-name-face)

          ;;  (,(rx "<<" (*? anything) ">>")
          ;;   (save-excursion (re-search-forward (rx (*? anything) "=")) (point))
          ;;   (re-search-backward (rx (| "let" "rec" "fix") (any space)))
          ;;   (0 font-lock-constant-face t))

          ;;  (,(rx "(" (group-n 1 (*? anything)) ")")
          ;;   (save-excursion (re-search-forward (rx (*? anything) "=")) (point))
          ;;   (re-search-backward (rx (| "let" "rec" "fix") (any space)))
          ;;   (1 font-lock-variable-name-face t))

          ;;  (,(rx "(" (| "type " "size ") (*? anything) ")")
          ;;   (save-excursion (re-search-forward (rx (*? anything) "=")) (point))
          ;;   (re-search-backward (rx (| "let" "rec" "fix") (any space)))
          ;;   (0 font-lock-type-face t))

          ;;  (,(rx (group-n 1 ":") (group-n 2 (*? anything)) (group-n 3 (| "," "=" ")")) )
          ;;   (save-excursion (re-search-forward (rx (*? anything) "=")) (point))
          ;;   (re-search-backward (rx (| "let" "rec" "fix") (any space)))
          ;;   (1 '() t)
          ;;   (2 font-lock-type-face t)
          ;;   (3 '() t))

          ;;  (,(rx (*? anything) (group-n 1 (eval ident)) (*? anything) )
          ;;   (save-excursion (re-search-forward (rx (*? anything) "=")) (point))
          ;;   nil
          ;;   (1 font-lock-variable-name-face t))
          ;; )



          (,(rx symbol-start (group-n 1 "fun") symbol-end)
           (1 font-lock-keyword-face)
           (,(rx "<<" (*? anything) ">>")
            (save-excursion (re-search-forward (rx (*? anything) "->")) (point))
            (re-search-backward (rx symbol-start "fun" symbol-end))
            (0 font-lock-constant-face t))

           (,(rx "(" (group-n 1 (*? anything)) ")")
            (save-excursion (re-search-forward (rx (*? anything) "->")) (point))
            (re-search-backward (rx symbol-start "fun" symbol-end))
            (1 font-lock-variable-name-face t))

           (,(rx "(" (| "type " "size ") (*? anything) ")")
            (save-excursion (re-search-forward (rx (*? anything) "->")) (point))
            (re-search-backward (rx symbol-start "fun" symbol-end))
            (0 font-lock-type-face t))

           (,(rx (group-n 1 ":") (group-n 2 (*? anything)) (group-n 3 (| "," "=" ")")) )
            (save-excursion (re-search-forward (rx (*? anything) "->")) (point))
            nil ;(re-search-backward (rx "let" (any space)))
            (1 '() t)
            (2 font-lock-type-face t)
            (3 '() t))
          )



;;           (,(rx (group-n 1 (: "lett" (? (: (+ (any space) "rec")))) (+ (any space)))
;;                 (group-n 2 (eval ident)) (*? anything) "=")
;;            (1 '(:weight bold))
;;            (2 font-lock-function-name-face)
;; ;           (,(rx (*? anything) "=")
;; ;            (save-excursion (up-list) (point))
;;  ;           (re-search-backward "let")
;;   ;          (0 font-lock-type-face))
;;            (,(rx "<<" (*? anything) ">>")
;;             (re-search-backward "let")
;;             nil
;;             (0 font-lock-constant-face t))
;;           )

;;          ("(\\(types\\) +\\(\\(?:\\sw\\|\\s_\\)+\\) +::"
;;           (0 font-lock-type-face))

;; ;          ("(\\(type\\) +\\(\\(?:\\sw\\|\\s_\\)+\\) +::"
;;          (,(rx "(" (group-n 1 "type") (+ (any space)) (group-n 2 (eval ident)) (* (any space)) "::")
;;     ;; fontify the `type' as keyword
;;     (1 font-lock-keyword-face)
;;     ;; fontify the function name as function
;;     (2 font-lock-function-name-face)
;;     ;; look for symbols after the `::', they are types
;;     ("\\_<\\(\\(?:\\sw\\|\\s_\\)+\\)\\_>"
;;      ;; set the limit of search to the current `type' form only
;;      (save-excursion (up-list) (point))
;;      ;; when we found all the types in the region (`type' form) go
;;      ;; back to the `::' marker
;;      (re-search-backward "::")
;;      ;; fontify each matched symbol as type
;;      (0 font-lock-type-face))
;;     ;; when done with the symbols look for the arrows
;;     ("->"
;;      ;; we are starting from the `::' again, so set the same limit as
;;      ;; for the previous search (the `type' form)
;;      (save-excursion (up-list) (point))
;;      ;; do not move back when we've found all matches to ensure
;;      ;; forward progress.  At this point we are done with the form
;;      nil
;;      ;; fontify the found arrows as variables (whatever...)
;;      (0 font-lock-variable-name-face t)))




          ;; ; Pattern binders (let/fix)
          ;; (,(rx (group-n 1 "let") (+ (any space))
          ;;       (group-n 2 (eval ident)) (* (any space)) ":"
          ;;       (group-n 3 (* (not (in "=")))) "=")
          ;;  (1 '(:weight bold))
          ;;  (2 font-lock-function-name-face)
          ;;  (3 font-lock-type-face))


          ;; ; Type binder (fun)
          ;; (,(rx (group-n 1 "fun") (+ (any space))
          ;;       (group-n 2 (eval ident)) (* (any space)) ":"
          ;;       (group-n 3 (eval (p-m n_par s_typ))) "->")
          ;;  (1 font-lock-keyword-face)
          ;;  (2 font-lock-variable-name-face)
          ;;  (3 font-lock-type-face))

          ; Size/Type abstraction/application
;          (,(rx "[type " (* (and "[" (eval n_bra) "]" (eval n_bra))) (eval n_bra) "]") . poly-annot)
;          (,(rx "[size " (* (and "[" (eval n_bra) "]" (eval n_bra))) (eval n_bra) "]") . poly-annot)
;          (,(rx "size" (* (any space)) (eval qvar) (* (any space)) "->") . poly-annot)
;          (,(rx "type" (* (any space)) (eval qvar) (* (any space)) "->") . poly-annot)

          ; Generic type annotation
          (,(rx "(" (*? anything) ":>" (group-n 1 (*? anything)) ")")
           (1 font-lock-type-face))
          (,(rx "[" (*? anything) ":>" (group-n 1 (*? anything)) "]")
           (1 font-lock-type-face))
          (,(rx ":" (group-n 1 (eval (p-m n_par n_par))) (in ")"))
           (1 font-lock-type-face))

          ; Interger constant
          (,(rx (not (in "a-z" "A-Z" "_" "0-9")) (group-n 1 (* (any digit))))
           (1 font-lock-constant-face))

          ; Sizes
          (,(rx  "<<" (* (not (in ">"))) ">>" ) . font-lock-constant-face)

          ; Builtins
          (,(rx "$" (eval ident)) . font-lock-builtin-face)

          ; Special identifiers
          (,(concat (rx symbol-start) x-types-regexp (rx symbol-end)) . font-lock-type-face)
          (,(concat (rx symbol-start) x-struct-regexp (rx symbol-end)) (0 '(:weight bold)))
          (,(concat (rx symbol-start) x-keywords-regexp (rx symbol-end)) . font-lock-keyword-face)
          (,(concat (rx symbol-start) x-constants-regexp (rx symbol-end)) . font-lock-constant-face)
;          (,(concat (rx symbol-start) x-functions-regexp (rx symbol-end)) . font-lock-builtin-face)

          ; Test markers
          (,(rx "//" (* (any space)) (| "FAIL" "SUCC"))
           (0 '(:inherit font-lock-comment-face :weight bold) t))

          )
        )
      )



(defvar surf-mode-syntax-table nil "Syntax table for `surf-mode'.")

(setq surf-mode-syntax-table
      (let ( (synTable (make-syntax-table)))
        ;; make char's syntax for C++ style comment “// …”
        (modify-syntax-entry ?/ ". 124b" synTable)
        (modify-syntax-entry ?\n "> b" synTable)
        ;; comment style “/* … */”
        (modify-syntax-entry ?* ". 23n" synTable)
        synTable))


;;;###autoload
(define-derived-mode surf-mode fundamental-mode "Surface Language mode"
  "Major mode for editing programmes in the surface language"
  (setq font-lock-defaults '((surf-mode-font-lock)))
  (set-syntax-table surf-mode-syntax-table)
  )

;; add the mode to the `features' list
(provide 'surf-mode)

