# Size inference formalization and test

This repository contains a light implementation that extends the materials
described in ARRAY-2023 submitted article 'Polymorphic Types with Polynomial
Sizes'.
The type checker supports two languages:
- the Core language that is closed to the one described in the article
- the Surface language that provides lighter notations and allow complete
  size parameter omission.
  
Both languages support extensions (anonymous variables, explicitly quantified
variables, coercions, local existential sizes, fix-point).


## Static analyzer building

Requirements: OCaml 4.11 or greater.
Compilation: `dune build` or `./build` will create a file `compiler.exe`

This static analyszer has two font-ends:
- Surface language: the higher level language with array syntax
- Core language: the internal language used for inference and checking.

By default, the static analyser dumps declarations' type scheme. Several options
affects the output:
- `--dump-elab` also prints terms after parameter insersion and elaboration
- `--no-dump`  clears all outputs


## Builtins

In the Core front-end, no infix operators are provided, instead, Builtins are
available. They are prefixed with '$': $add, $sub, ...


## Examples

Each file of the `ex` folder contains its compilation directive.

Top-level declarations may be followed by a special comment `// SUCC` or
`// FAIL` to indicate the wether the static analysis is supposed to succeed or
not, and prevent checking interruption in case of expected errors.


## Misc

File `syntax.el` provides (basic) Emacs color modes for both the Core language
(`core-mode`) (to use in compilation output) and the Surface language
(`surf-mode`).

