(** -------- Concrete Syntax Tree of the Core language -------- **)

open Std

type ident = string
type var = ident loc

type 'a opt = 'a opt' loc
and 'a opt' = 'a loc  option


type eta = eta' opt and eta' =
  | S_var of var
  | S_int of int loc
  | S_add of eta pair
  | S_mul of eta pair

type tau = (eta list option * tau' opt) loc and tau' =
  | T_var   of var
  | T_base  of typ
  | T_index of eta
  | T_arrow of tau pair

type 'a sized = 'a * eta
type 'a typed = 'a * tau
type 'a named = var * 'a

type gen = (var,var) param list

type exp = exp' loc and exp' =
  | E_var of var
  | E_cst of cst
  | E_coe of exp typed
  | E_typ of exp typed
  | E_app of exp * app
  | E_abs of abs * exp
  | E_let of decl * exp
  | E_exS of exp named * exp
  | E_cnd of exp * exp pair
  | E_err

and app =
  | A_exp of exp
  | A_size of eta list loc
  | A_index of exp list loc

and abs =
  | I_var   of var typed
  | I_size  of var list loc
  | I_index of var sized list loc
  | I_gen   of gen loc

and decl = location option * (var * gen * tau * exp)

and top_decl = decl loc tested

