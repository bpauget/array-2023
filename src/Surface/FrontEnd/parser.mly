(** -------- Direct parser of the Core language -------- **)

%{
    open Std
    open Cst

    let (!:) = fst
    let (!) = snd

    let none l = None, l
    let some (o,l) = Some (o,l),l
    let dflt d = function
      | None, l -> d,l
      | Some o, _ -> o


    (* Fixpoint instanciation *)
    let v ((x,_,_,_),_) = E_var x, !x

    (* Syntactical sugar for sizes:  a - b = a + (-1)*b *)
    let minus a l b =
      S_add (a,some (S_mul (some (S_int (-1,l),l), b), l <-> !b))


    let some t = (Some [],some t), !t
    let none l = (None   ,none l),  l

    (* Syntactical sugar for types:  [s]t = [s] -> t *)
    let array s l t =
      T_arrow (some (T_index s,l), t)

    let position p =
      { l_file = p.Lexing.pos_fname;
        l_lbeg = p.pos_lnum;
        l_lend = p.pos_lnum;
        l_cbeg = p.pos_bol;
        l_cend = p.pos_bol; }


    let intro l t e =
      List.fold_right (fun i e ->
        E_abs (!:i,e), !i <-> !e) l
        (E_typ (e,t), !t <-> !e)

    let get_op (((x,_),_),l) = E_var (x,l), l

    let unop o a = E_app (get_op o, A_exp a)
    let binop a o b = E_app ((E_app (get_op o, A_exp a), !a <-> !o), A_exp b)

    let coer t e = E_coe (e, t), !e <-> !t
    let pipe f e = E_app (f, A_exp e), !e <-> !f
    let f_pipe l f =
      let n = "_x",l in
      let e = f (E_var n, l) in
      E_abs (I_var (n,none !n),e), l <-> !e

    let f_pipe f =
      let l = snd (List.hd f) in
      let n = "_x",l in
      let e = List.fold fst f (E_var n, l) in
      E_abs (I_var (n,none !n),e), l <-> !e

%}

(**** Tokens ****)
%token <bool Std.loc> TEST
%token <Std.typ Std.loc> TYP
%token <Std.location> LET REC FIX FUN IN ARROW EQUAL SIZE TYPE IF THEN ELSE PIPE
%token <Std.location> LPAREN RPAREN LBRACK RBRACK LANGLE RANGLE LCURLY RCURLY
%token <Std.location> COLON COERCE COMMA UNDERSCORE DOT
%token <int    Std.loc> INT
%token <bool   Std.loc> BOOL
%token <string Std.loc> IDENT BUILTIN LETTER
%token <Std.location> LT GT LE GE NE
%token <Std.location> PLUS STAR MINUS SLASH PERCENT NOT
%token <Std.location> BARBAR AMPAMP HAT
%token EOF

(**** Precedence ****)
%left PIPE
%left BARBAR
%left AMPAMP
%left HAT
%nonassoc EQUAL NE LT GT LE GE
%left PLUS MINUS
%left STAR SLASH PERCENT
%nonassoc NOT

%left LPAREN LANGLE APP INT BOOL IDENT BUILTIN DOT
%left LBRACK

%start <Cst.top_decl list> program
%%


(**** Utils ****)
%inline empty: { position $endpos }
%inline paren (item): l=LPAREN i=item r=RPAREN { i, l <-> r } // ( . )
%inline brack (item): l=LBRACK i=item r=RBRACK { i, l <-> r } // [ . ]
%inline curly (item): l=LCURLY i=item r=RCURLY { i, l <-> r } // { . }
%inline angle (item): l=LANGLE i=item r=RANGLE { i, l <-> r } // < . >

%inline e_list(SEP,item): separated_list(SEP,item) { $1 }

%inline opt (item): // Inlined explicit placeholder
  | l=UNDERSCORE { None  ,  l }
  | i=item       { Some i, !i }

%inline impl (item): // Inlined implicit item
  | l=empty      { None  , l  }
  | i=item       { Some i, !i }

%inline o_ann (SEP,item): // Separated optional annotation
  | l=empty      { None, l }
  | SEP i=item   { i       }
%inline p_ann (SEP,item): // Separated optional annotation
  | l=empty      { none l }
  | SEP i=item   { i      }

%inline param(eta,tau):
  | SIZE s=eta* { List.map (fun s -> Size s) s }
  | TYPE t=tau* { List.map (fun t -> Type t) t }

avr: LETTER     { $1            }   // Anonymous variable
var: IDENT      { $1            }   // Normal variable
ivr: opt(IDENT) { $1 |> dflt "" }   // Ignorable variable

cst:
  | i=INT  { C_int  !:i, !i }
  | b=BOOL { C_bool !:b, !b }

(**** Sizes *****)
eta: opt(eta_) { $1 }
eta_:
  | n=INT                { S_int n    , !n        }
  | v=var                { S_var v    , !v        }
  | v=avr                { S_var v    , !v        }
  | a=eta PLUS b=eta     { S_add (a,b), !a <-> !b }
  | a=eta STAR b=eta     { S_mul (a,b), !a <-> !b }
  | a=eta l=MINUS b=eta  { minus a l b, !a <-> !b }
  | e=paren(eta_)        { !: !:e     , !e        }


(**** Types *****)
tau: t=opt(tau_) { (Some [],t),!t }
  | t=paren(fnc)       { !: !:t        , !t        }
fnc:
  | t=opt(fnc_) { (None, t), !t }
  | l=LT s=e_list(COMMA,eta) GT ARROW t=opt(fnc_) { (Some s,t), l <-> !t }
tau_:
  | b=TYP               { T_base !:b    , !b }
  | v=var               { T_var v       , !v }
  | v=avr               { T_var v       , !v }
  | s=brack(eta)        { T_index !:s   , !s        }
  | s=brack(eta) t=tau  { array !:s !s t, !s <-> !t }
fnc_:
  | t=tau_             { t }
  | t=tau ARROW u=fnc  { T_arrow (t, u), !t <-> !u }


(**** Buitlin operators ****)
%inline
binop:
  | EQUAL   { Builtins.eq , $1 }
  | NE      { Builtins.ne , $1 }
  | LT      { Builtins.lt , $1 }
  | GT      { Builtins.gt , $1 }
  | LE      { Builtins.le , $1 }
  | GE      { Builtins.ge , $1 }
  | HAT     { Builtins.xor, $1 }
  | BARBAR  { Builtins.ior, $1 }
  | AMPAMP  { Builtins.bnd, $1 }
  | PLUS    { Builtins.add, $1 }
  | MINUS   { Builtins.sub, $1 }
  | STAR    { Builtins.mul, $1 }
  | SLASH   { Builtins.div, $1 }
  | PERCENT { Builtins.rem, $1 }

%inline
unop:
  | NOT     { Builtins.not, $1 }


(**** Introduction ****)
intro (tau, SEP): l=abs+ t=p_ann(COLON,tau) SEP e=exp_ { intro l t e }

abs:
  | x=ivr { I_var (x,none !x), !x }
  | x=paren(pair(ivr,p_ann(COLON, fnc))) { I_var !:x, !x}
  | x=brack(e_list(COMMA,pair(ivr,o_ann(LT,eta)))) { I_index x, !x }
  | x=angle(e_list(COMMA,ivr)) { I_size x, !x}
  | v=paren(param(var,var)) { I_gen v, !v }

(**** Expressions ****)
exp:
  | l=DOT                   { E_err          , l         }
  | x=var                   { E_var x        , !x        }
  | x=BUILTIN               { E_var x        , !x        }
  | c=cst                   { E_cst !:c      , !c        }
  | o=unop e=exp            { unop o e       , !o <-> !e }
  | a=exp o=binop b=exp     { binop a o b    , !a <-> !b }
  | f=exp a=app   %prec APP { E_app (f,!:a)  , !f <-> !a }
  | e=exp PIPE f=exp         { pipe f e   }
  | e=paren(coerce)         { !: !:e         , !e        }
  | e=paren(exp_)           { !: !:e         , !e        }
  | o=paren(binop)          { !: (get_op !:o), !o        }
  | o=paren(unop)           { !: (get_op !:o), !o        }
exp_:
  | e=exp { e }
  | l=FIX d=decl                              { E_let ((Some l,!:d),v d), l <-> !d }
  | l=LET r=REC? d=decl IN e=exp_             { E_let ((r     ,!:d),e)  , l <-> !e }
  | l=LET SIZE v=ivr EQUAL s=exp_ IN e=exp_   { E_exS ((v,s),e)         , l <-> !e }
  | l=FUN e=intro(tau,ARROW)                  { !:e                     , l <-> !e }
  | l=IF c=exp_ THEN t=exp_ ELSE f=exp_       { E_cnd (c,(t,f))         , l <-> !f }
  | l=pipes+  { f_pipe l }
(*  | l=PIPE f=pipes                            { f_pipe l f }
pipes:
  | f=exp   %prec PIPE { pipe f }
  | f=pipes PIPE g=exp { f <|> pipe g }
  | f=pipes COERCE t=tau { f <|> coer t }
*)
pipes:
  | l=PIPE e=exp    { pipe e, l }
  | l=COERCE t=tau  { coer t, l }

%inline app:
  | e=exp                      { A_exp e, !e }
  | s=angle(e_list(COMMA,eta)) { A_size s, !s }
  | e=brack(e_list(COMMA,idx)) { A_index e, !e }

idx:
  | e=exp              { e }
  | e=exp COERCE s=eta { E_coe (e, some (T_index s,!s)), !e <-> !s }

gen: l=terminated(param(var,var),DOT)* { List.concat l }


coerce:
  | e=exp COERCE t=fnc { E_coe (e,t), !e <-> !t }
  | e=exp COLON  t=fnc { E_typ (e,t), !e <-> !t }

decl:
  | x=ivr EQUAL e=exp_ { (x,[],none !x,e), !x <-> !e }
  | x=ivr COLON g=gen t=fnc EQUAL e=exp_ { (x,g,t,e), !x <-> !e }
  | x=var e=intro(fnc,EQUAL) { (x,[],none !x,e), !x <-> !e }


(**** Value declaration ****)
let_decl: l=LET r=REC? d=decl   { (r,!:d), l <-> !d }

top_decl: d=let_decl t=TEST? { d,t }


(**** Entry point *****)
program: d=top_decl* EOF { d }
