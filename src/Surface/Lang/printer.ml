open Std
open Ast
open Format



type assoc = NA | LtoR | RtoL

let prec_level = function
  | E_err         -> 16, NA
  | E_var _       -> 16, NA
  | E_cst _       -> 16, NA
  | E_coe _       -> 0, NA
  | E_typ _       -> 0, NA
  | E_app _       -> 15, LtoR
  | E_cnd _       -> 4, LtoR
  | E_let _       -> 3,RtoL
  | E_exS _       -> 3,RtoL
  | E_abs _       -> 3,RtoL
  | E_gen _       -> 3,RtoL
let atom = 6

(*
let prec_level_t = function
  | T_var _ -> 16, NA
  | T_base _ -> 16, NA
  | T_size _ -> 16, NA
  | T_index _ -> 16, NA
  | T_arrow (T_index _, t) -> 15,RtoL
  | T_arrow _ -> 14, RtoL

let base_type_s = function
  | T_int -> "int"
  | T_bool -> "bool"


let var p (v,_) = fprintf p "%s" (Names.to_string v)

let eta vars p = Size.print (snd vars) string_of_int p


let tau vars prec p t =
  let level,assoc = prec_level_t (T_arrow (T_base T_int, T_base T_int)) in
  let lP,rP = match assoc with
    | LtoR -> level  , 1+level
    | RtoL -> level+1,   level
    | NA   -> level+1, 1+level
  in
  let rec tau sizes p = function
    | T_arrow (T_size s, t) -> tau (s::sizes) p t
    | t -> fprintf p "<%a> ->@ %a" (List.pp "," (eta vars)) (List.rev sizes) (tau' rP) t
  and tau' prec p t =
    let level,assoc = prec_level_t t in
    let lP,rP = match assoc with
      | LtoR -> level  , 1+level
      | RtoL -> level+1,   level
      | NA   -> level+1, 1+level
    in
    let f p = function
      | T_var v -> fprintf p "%s" (fst vars v)
      | T_base t -> fprintf p "%s" (base_type_s t)
      | T_size s -> fprintf p "<%a>" (eta vars) s
      | T_index s -> fprintf p "[%a]" (eta vars) s
      | T_arrow (T_index s,t) -> fprintf p "[%a]%a" (eta vars) s (tau' rP) t
      | T_arrow (T_size _,_) as t -> tau [] p t
      | T_arrow (t,u) -> fprintf p "%a -> %a" (tau' lP) t (tau' rP) u
    in
    fprintf p (if level < prec  then "(@[<hov>%a@])" else "%a") f t
  in
  tau' prec p t


let fnc vars p = fprintf p "@[<hov>%a@]" (tau vars 15)
let tau vars p = fprintf p "@[<hov>%a@]" (tau vars 0)
*)

let tau = Core.Printer.tau
let eta = Core.Printer.eta
let var = Core.Printer.var


let param vars eta tau p = function
  | Size s -> fprintf p "size %a" (eta vars) s
  | Type t -> fprintf p "type %a" (tau vars) t

let poly vars f_eta f_tau p = function
  | P_var _ -> ()
  | P_def l ->
     if not (List.is_empty (fst l)) then
       fprintf p "[%a]" (List.pp ", " (param vars
                                         (fun v p -> fst <|> f_eta <|> eta v p)
                                         (fun v p -> fst <|> f_tau <|> tau v p))) (fst l)


let pat vars p (abs,t) =
  let p_end o = if o <> "" then fprintf p ". " in
  let p_var k v o =
    if k = o then fprintf p " %s" v
    else (p_end o; fprintf p "%s %s" k v);
    k
  in
  List.fold (param_fold
               fst#.(fst vars)#.(p_var "size")
               fst#.(snd vars)#.(p_var "type")) abs ""
  |> p_end;
  tau vars p (fst t)

let rec exp vars prec p e =
  let eta v p s = eta v p (fst s) in
  let level,assoc = prec_level (fst e) in
  let lP,rP = match assoc with
    | LtoR -> level  , 1+level
    | RtoL -> level+1,   level
    | NA   -> level+1, 1+level
  in

  let idx p = function
    | E_coe (e,(T_val (T_int (R_idx s),[]),l)),_ -> fprintf p "%a :> %a" (exp vars 5) e (eta vars) (s,l)
    | e -> exp vars 5 p e
  in
  let app p = function
    | A_exp e -> exp vars rP p e
    | A_size l -> fprintf p "<%a>" (List.pp ", " (eta vars)) (fst l)
    | A_index l -> fprintf p "[%a]" (List.pp ", " idx) (fst l)
  in

  let f p = function
    | E_err       -> fprintf p "."
    | E_var (x,i) -> fprintf p "%a%a" var x (ins vars) i
    | E_cst c     -> fprintf p "%s" (cst_s c)
    | E_coe (e,t) -> fprintf p "%a:>@;%a" (exp vars lP) e (tau vars) (fst t)
    | E_typ (e,t) -> fprintf p  "%a:@;%a" (exp vars lP) e (tau vars) (fst t)
    | E_app (f,a) -> fprintf p "%a@;%a" (exp vars lP) f app a
    | E_abs _ -> fprintf p "fun%a" (intro vars "->") e
    | E_gen _ -> fprintf p "fun%a" (intro vars "->") e
    | E_let (d,f) -> fprintf p "@[<v>@[<hov>%a@;in@]@;@]%a" (decl vars) d (exp vars rP) f
    | E_exS ((v,e),f) -> fprintf p "@[<hov>@[<hov 2>let size %s =@;%a@;in@]@;@]%a" (fst vars (fst v))
                             (exp vars 1) e (exp vars rP) f
    | E_cnd (c,(t,f)) -> fprintf p "@[<v>@[<hov 2>case %a@]@;@[<hov 2>then %a@]@;@[<hov 2>else %a@]@]"
                            (exp vars 0) c (exp vars 0) t (exp vars 0) f
  in
  fprintf p (if level < prec  then "(@[<hov>%a@])" else "%a") f (fst e)

and intro vars sep p =
  let abs p = function
    | I_var (x,t) -> fprintf p "(%a: %a)" var x (tau vars) (fst t)
    | I_size l -> fprintf p "<%a>" (List.pp "," (fun p (x,s) -> fprintf p "%a=%a" var x (eta vars) (fst s))) l
    | I_index (x,s) -> fprintf p "[%a<%a]" var x (eta vars) (fst s)
  in
  function
  | E_abs (a,e),_ -> fprintf p "@ %a%a" abs a (intro vars sep) e
  | E_gen (g,e),_ -> fprintf p "@ %a%a" (gen vars) (P_def g) (intro vars sep) e
  | e -> fprintf p " %s@;%a" sep (exp vars 0) e

and ins vars = poly vars id id
and gen vars = poly vars Size.unit Type.unit


and decl vars p (r,g,x,t,e) =
  fprintf p "@[<v 2>@[<hov 2>let%s %a%a: %a =@]@;@[<hov >%a@]@]"
    (if r = None then "" else " rec")
    var x (gen vars) g (pat vars) t
    (exp vars 1) e



let top_decl p ((d,_),_) =
  let _,t = TVar.ids () in
  let _,s = SVar.ids () in
  decl (s,t) p d

let implementation p =
  fprintf p "@[<v>%a@]@." (List.pp "@;@;" top_decl)
