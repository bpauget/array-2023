
open Std
open Ast
module A = Core.Ast


let eta s = s
let tau t = t


let loc fn = map_fst fn

let poly eta tau = function
  | P_var v -> P_var v
  | P_def l -> P_def (map_fst (List.map (param_map eta tau)) l)

let (let+) e = e


let ins_var = List.map (param_map (map_fst Size.unit) (map_fst Type.unit))

let rec exp (exp,(_,loc)) = exp' loc exp, loc
and exp' l : exp' -> A.exp' = function
  | E_var (v,i)     -> E_var (v, ins i)
  | E_cst c         -> E_cst c
  | E_coe (e,t)     -> E_coe (exp e, loc tau t)
  | E_typ (e,t)     -> E_typ (exp e, loc tau t)
  | E_app (f,a)     -> app (exp f) a
  | E_abs (i,e)     -> abs (exp e) i
  | E_gen (g,e)     -> E_gen (gen g, exp e)
  | E_let (d,e)     -> E_let (decl d, exp e)
  | E_exS ((v,s),e) -> E_exS ((v,exp s), exp e)
  | E_cnd (e,p)     -> E_cnd (exp e, Pair.map exp p)
  | E_err           -> E_err

and abs e : _ -> A.exp' = function
  | I_var (x,t) -> E_abs ((x, loc tau t), e)
  | I_size l -> List.fold_right (fun (x,(s,l)) e -> A.E_abs ((x,(size (eta s), l)), e), snd x <-> snd e) l e
                |> fst
  | I_index (x,s) -> E_abs ((x,(index (fst s), snd s)), e)



and app f : _ -> A.exp' = function
  | A_exp e -> E_app (f, exp e)
  | A_size l -> List.fold (fun s f ->  A.E_app (f, (A.E_vaS (loc eta s), snd s)), snd f <-> snd s) (fst l) f
                |> fst
  | A_index l -> List.fold idx#.(fun i f -> A.E_app (f, i), snd f <-> snd i) (fst l) f
                 |> fst

and idx = function
  | E_coe (e, (T_val (T_int (R_idx n), []),_)), _ ->
     E_typ (exp e, (index n, ~?e)), ~?e
  | e -> E_typ (exp e, (index (Size.unit ~!SVar.create), ~?e)), ~?e

and ins i = poly (loc eta) (loc tau) i
and gen g = g

and decl (r,g,x,(v,t),e) =
  let l = ~?e in
  let d = P_def (v,l),x,loc tau t,exp e in
  let i = P_def (ins_var v,l) in
  (g,x,(Type.unit ~!TVar.create,l),
   (if Option.is_some r then A.E_fix (d, i), l
    else A.E_let (d, (E_var (x,i),l)), l))


let top_decl (d,a) = decl d, a

