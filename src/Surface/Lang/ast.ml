(** -------- Abstract Syntax Tree of the Core language -------- **)


(**
   Conventions:
   - To make extensions of the language easier, most
     pattern matchings are exhaustive (causing a warning
     if type is extended). The remaining generic cases
     are marked with (*! DFLT typ !*) comments (where
     typ is the names of the type) for easy finding.
 **)

open Std


(* Expression identifiers *)
module EVar = Vars (struct let shft = 0 let pref = "e" end)
module ESet = EVar.Set
module EMap = EVar.Map


type pat = (Size.v loc, Type.v loc) param list * tau loc


type ins = (Size.t loc, Type.t loc) param list loc
type gen = (Size.v loc, Type.v loc) param list loc


(* Variables *)
type var = ident loc
type 'a typed = 'a * tau loc
type 'a sized = 'a * eta loc

(* Expressions *)
type exp = exp' * EVar.t loc and exp' =
  | E_var of var * ins pvar
  | E_cst of cst
  | E_coe of exp typed
  | E_typ of exp typed
  | E_app of exp * app
  | E_abs of abs * exp
  | E_gen of gen * exp
  | E_let of decl * exp
  | E_exS of (Size.v loc * exp) * exp
  | E_cnd of exp * exp pair
  | E_err

and app =
  | A_exp of exp
  | A_size of eta loc list loc
  | A_index of exp list loc

and abs =
  | I_var of var typed
  | I_size of var sized list
  | I_index of var sized


(* Inner declarations *)
and decl = location option * gen pvar * var * pat * exp

(* Top level declarations contains:
   - the inner declaration
   - a test directive (FAIL or SUCC comment)
 *)
type top_decl = decl * (SSet.t * TSet.t)

type implementation = top_decl tested list



let p_vars () = snd (TVar.ids ()), snd (SVar.ids ())

let (~?) e = snd#.snd e
let decl_var (_,_,x,_,_) = x
