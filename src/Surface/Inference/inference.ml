

open Std
open Ast

let (!!) l = List.length l


(* Poly variables *)
module AVar = Vars (struct let shft = 0 let pref = "" end)
module ASet = AVar.Set
module AMap = AVar.Map


type tau = ar * ty
and ty =
  | T_var of ty option ref
  | T_fun of tau pair
and ar =
  | A_var of AVar.t
  | A_int of int


let rec tau : Std.tau -> _ = function
  | T_var _ -> A_var ~!AVar.create, T_var (ref None)
  | T_val (T_fun, [T_val (T_int R_siz s, []); t]) ->
     begin match tau t with
     | A_var _, t -> A_int 1, t
     | A_int n, t -> A_int (n+1),t
     end
  | T_val (T_fun, [d;c]) -> A_int 0, T_fun (tau d, tau c)
  | _ -> A_int 0, T_var (ref None)


let tau = fst#.tau


let rec deref = function
  | T_var { contents = Some t } -> deref t
  | t -> t
let undef () = A_var ~!AVar.create,
               T_var (ref None)
let new_a () = A_var ~!AVar.create
let new_t () = T_var (ref None)


let base = A_int 0, new_t ()


type st =
  { cstr : ar pair loc list
  ; vars : tau loc AMap.t
  ; etau : tau EMap.t
  ; eari : ar EMap.t }

let cstr () = F.rw (fun s -> s.cstr) (fun l s -> { s with cstr = l }) (list nop)
let vars () = F.rw (fun s -> s.vars) (fun m s -> { s with vars = m }) (AMap.prop nop)
let etau () = F.rw (fun s -> s.etau) (fun m s -> { s with etau = m }) (EMap.prop nop)
let eari () = F.rw (fun s -> s.eari) (fun m s -> { s with eari = m }) (EMap.prop nop)

let empty =
  { cstr = List.empty
  ; vars = AMap.empty
  ; etau = EMap.empty
  ; eari = EMap.empty }


include StateMonad.Intro (StateMonad.Make (struct type t = st end))
let add_exp eid t = effect (etau #+ eid t)
let add_ari eid a = effect (eari #+ eid a)
let add_var v l t = effect (vars #+ v (t,l))
let add_ctr l t u = effect (cstr #+ ((t,u),l))


  








include StateMonad.Intro (StateMonad.Make (struct type t = ar pair loc list *
                                                             (tau EMap.t * ar EMap.t) end))




let uni loc = function
  | T_var v when !v = None ->
     let p = undef (), undef () in
     let () = v := Some (T_fun p) in
     return p

  | T_var _ -> assert false
  | T_fun p -> return p

let rec occurs v = function
  | T_var v' -> if v == v' then true
                else Option.map (occurs v) !v' |> Option.value ~default:false
  | T_fun (c,d) -> occurs v (snd c) || occurs v (snd d)

let rec sub_type loc (a2,t2) (a1,t1) =
  match deref t1, deref t2 with
  | T_var _, T_var _ -> effect (map_fst (List.cons ((a1,a2),loc)))

  | T_fun (d1,c1), T_fun (d2,c2) ->
     let* () = sub_type loc d1 d2 in
     let* () = sub_type loc c2 c1 in
     effect (map_fst (List.cons ((a1,a2),loc)))

  | T_var v, t when !v = None && not (occurs v t) ->
     let p = undef (), undef () in
     let () = v := Some (T_fun p) in
     sub_type loc (a2,t) (a1,T_fun p)

  | t, T_var v when !v = None && not (occurs v t) ->
     let p = undef (), undef () in
     let () = v := Some (T_fun p) in
     sub_type loc (a2,T_fun p) (a1,t)

  | _ -> effect id
(*
  | t1,t2 -> let* p1 = uni loc t1 in
             let* p2 = uni loc t2 in
             sub_type loc (a2,T_fun p2) (a1,T_fun p1)
*)





let add_var x t = map_snd (IMap.add (fst x) t)


let add_exp eid a = effect (map_snd (map_snd (EMap.add eid a)))
let add_ari eid t = effect (map_snd (map_fst (EMap.add eid t)))


let eid (_,(eid,_)) = eid

let var (g_env,l_env) (x,l) =
  if IMap.mem x l_env
  then return (IMap.get l_env x)
  else return (IMap.get g_env x |> fun ((_,t),_) -> tau (t,l))

let rec exp env (exp,(eid,loc)) =
  let* a,t = exp' env eid loc exp in
  let+ () = add_exp eid a in
  a,t

and exp' env eid loc =  function
  | E_var (x,_) -> var env x
  | E_cst c -> return base
  | E_coe (e,t) | E_typ (e,t) ->
     let t = tau t in
     let+ _ = exp env e >>= sub_type loc t in
     t
  | E_app (e,a) -> app env e a
  | E_abs (a,e) -> abs env e a
  | E_gen (_,e) -> exp env e

  | E_let (d,e) ->
     let* env = decl env d in
     exp env e

  | E_exS ((_,s),e) ->
     let* _ = exp env s >>= sub_type ~?s base in
     exp env e

  | E_cnd (c,(t,f)) ->
     let v = new_a (), new_t () in
     let+ _ = exp env c >>= sub_type ~?c base
     and+ _ = exp env t >>= sub_type ~?t v
     and+ _ = exp env f >>= sub_type ~?f v in
     v

  | E_err -> return ~!undef


and app env fn = function
  | A_exp e ->
     let* _,t = exp env fn in
     let* d,c = uni (snd fn) (deref t) in
     let* () = add_ari (eid e) d in
     let+ () = exp env e >>= sub_type ~?e d in
     c

  | A_size (s,l) ->
     let* a,t = exp env fn in
     let+ () = sub_type l (A_int !!s,new_t()) (a,t) in
     A_int 0, t

  | A_index (i,l) ->
     exp env fn >>=
       foldM (fun e (_,t) ->
           let* d,c = uni (snd fn) (deref t) in
           let* () = sub_type l d base in
           let+ () = exp env e >>= sub_type ~?e base in
           c
         ) i


and abs env e = function
  | I_var (x,t) ->
     let t = tau t in
     let env = add_var x t env in
     let* t' = exp env e in
     let+ () = add_ari (eid e) t' in
     A_int 0, T_fun (t,t')

  | I_size l ->
     let env = List.fold fst#.(fun x -> add_var x base) l env in
     let t = new_t () in
     let+ _ = exp env e >>= sub_type ~?e (A_int 0, t) in
     A_int !!l, t

  | I_index (x,s) ->
     let t = tau (index (fst s), snd s) in
     let env = add_var x t env in
     let* t' = exp env e in
     let+ () = add_ari (eid e) t' in
     A_int 0, T_fun (t,t')


and decl env (r,_,x,p,e) =
  let t = tau (snd p) in
  let* () = add_ari (eid e) t in
  let env' = add_var x t env in
  let+ _ = exp (if Option.is_some r then env' else env) e >>= sub_type ~?e t in
  env'






module Inser = struct
  let (!) = function
    | A_int n -> n
    | A_var v -> 0

  let (!?) = function
    | T_fun (_,t) -> t
    | _ -> assert false

  let (=>) a b = A_int 0, T_fun (a,b)

  let rec exp arit (exp,(eid,loc) as e)=
    expl arit (EMap.get (fst arit) eid) e

  and expl arit (ar,ty) (exp,(eid,loc)) =
    let ar' = EMap.get (snd arit) eid in
    if !ar' = !ar then exp' arit (ar,ty) exp, (eid,loc)
    else let () = assert (!ar = 0) in
         E_app ((exp' arit (ar',ty) exp, (eid,loc)),
                A_size ((fun l -> (Size.unit ~!SVar.create,loc) :: l)#* !ar' [],loc)),
         (EVar.create (), loc)

  and exp' : _ -> tau -> _ = fun arit tau -> function
    | E_err as e -> e
    | E_var _ as e -> e
    | E_cst _ as e -> e
    | E_coe (e,t) -> E_coe (expl arit tau e, t)
    | E_typ (e,t) -> E_typ (expl arit tau e, t)
    | E_app (e,a) -> app arit tau e a
    | E_abs (a,e) -> abs arit tau e a
    | E_gen (l,e) -> E_gen (l, expl arit tau e)
    | E_let (d,e) -> E_let (decl arit d, expl arit tau e)
    | E_exS (s,e) -> E_exS (map_snd (expl arit base) s, expl arit tau e)
    | E_cnd (c,p) -> E_cnd (expl arit base c, Pair.map (expl arit tau) p)


  and app arit (ar,ty) fn = function
    | A_exp e ->
       let tau = EMap.get (fst arit) (eid e) in
       E_app (expl arit (A_int 0, T_fun (tau,(ar,ty))) fn,
              A_exp (expl arit tau e))

    | A_size (s,l) ->
       assert (!ar = 0);
       E_app (expl arit (A_int !!s,ty) fn,
              A_size (s,l))

    | A_index (i,l) ->
       E_app (expl arit (List.fold (cst base)#.(=>) i (ar,ty)) fn,
              A_index (List.map (expl arit base) i, l))

  and abs arit (ar,ty) e = function
    | I_var x   -> assert (!ar =  0 ); E_abs (I_var x, expl arit !?ty e)
    | I_size l  -> assert (!ar = !!l); E_abs (I_size l, expl arit (A_int 0,ty) e)
    | I_index x -> assert (!ar =  0 ); E_abs (I_index x, expl arit !?ty e)

  and decl arit (r,g,x,p,e) =
    (r,g,x,p,exp arit e)

end

(* n <: 0: ok *)
(* n <: n: ok *)
(* n <: p: ko *)

let nece () = f_id #@ (AMap.prop ~default:IMap.empty (IMap.prop ~default:[] (list nop))) ()

let necessary = function
  | (A_var v, A_int n), l when n <> 0 -> nece #.. v #.. n #+ l
  | (A_int 0, A_var v), l -> nece #.. v #.. 0 #+ l
  | _ -> id

let sufficient = function
  | (A_int n, A_var v), l when n <> 0 -> nece #.. v #.. n #+ l
  | _ -> id


let filter = function
  | (a,b),_ when a = b -> None
  | (_,A_int 0),_ -> None
  | (A_int n, A_int p),l ->
     r_error l "Size parameter mismatch: %d expected, %d encountered" n p
  | c -> Some c


let subst subst =
  map_fst (Pair.map (function
               | A_var v -> AMap.find ~default:(A_var v) v subst
               | a -> a))


let pv = snd ~!AVar.ids
let p_ar p = function
  | A_var v -> Format.printf "%s" (pv v)
  | A_int i -> Format.printf "%d" i


let rec solve sub cstrs =(*
  List.iter fst#.(fun (a,b) ->
    Format.printf " %a<:%a" p_ar a p_ar b) cstrs;
  Format.printf "@.";*)
  let cstrs = List.filter_map (subst sub)#.filter cstrs in
  let nec =
    AMap.empty
    |> List.fold necessary cstrs
    |> AMap.mapi (fun a m ->
           if IMap.cardinal m = 1 then A_int (ISet.choose (IMap.keys m))
           else let () = p_error dummy_loc "Incompatible arity used" in
                let () = IMap.iter (fun i ->
                             List.iter (fun l -> p_note l "Used here with arity %d" i)) m in
                raise Error)
  in
  (* AMap.iter (fun v a -> Format.printf "Nec %s => %a@." (pv v) p_ar a) nec; *)
  if not (AMap.is_empty nec) then solve (AMap.union' sub nec) cstrs else
    let suf =
      AMap.empty
      |> List.fold sufficient cstrs
      |> AMap.map (fun m ->
             if IMap.cardinal m = 1 then A_int (ISet.choose (IMap.keys m))
             else A_int 0)
    in
    (* AMap.iter (fun v a -> Format.printf "Suf %s => %a@." (pv v) p_ar a) suf; *)
    if not (AMap.is_empty suf) then solve (AMap.union' sub suf) cstrs else
      sub


let top_decl env (d,avars) =

  let env',(cstrs,(t,a)) = init ([],(EMap.empty,EMap.empty)) (decl env d) in

  let subst = solve AMap.empty cstrs in

  let rec map_a = function
    | A_var v when AMap.mem v subst -> AMap.find v subst |> map_a
    | a -> a
  in
  let rec map_t (a,t) = map_a a, map_s (deref t)
  and map_s = function
    | T_fun p -> T_fun (Pair.map map_t p)
    | t -> t
  in

  let arit =
    EMap.map map_t t,
    EMap.map map_a a
 in

  env', (Inser.decl arit d, avars)

(*
  let a_sub,t_sub = solve (List.map (fun (((a1,t1),(a2,t2)),l) -> [A ((a1,a2),l);
                                                                   T ((t1,t2),l)]) cstrs |> List.concat) in
*)







let init_env =
  IMap.empty
  |> List.fold (fun ((s,x),t) ->
         IMap.add x (map_snd Elaboration.tau t,None)
       ) Builtins.builtins
