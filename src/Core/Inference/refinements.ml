
open Std
open Base
open Reso


type bound =
  { b_var: int Type.Map.t
  ; b_siz: (int * Size.t) List.t
  ; b_idx: (int * Size.t) List.t
  ; b_top: bool }

let empty =
  { b_var = Type.Map.empty
  ; b_siz = List.empty
  ; b_idx = List.empty
  ; b_top = false }


type 'a dsc = (bound pair * int tagged) Type.Map.t * Ins.t


let insert_size (scp,s) l = if List.exists snd#.(Size.equal s) l then l else (scp,s)::l

let b_var () = F.rw (fun b -> b.b_var) (fun s b -> { b with b_var = s }) (Type.map nop)
let b_siz () = F.rw (fun b -> b.b_siz) (fun l b -> { b with b_siz = l }) F.(prop |> p_add (a1 insert_size))
let b_idx () = F.rw (fun b -> b.b_idx) (fun l b -> { b with b_idx = l }) F.(prop |> p_add (a1 insert_size))
let b_top () = F.rw (fun b -> b.b_top) (fun l b -> { b with b_top = l }) F.(prop |> p_add (a1 (||)))



let bounds () = f_fst #@ (Type.Map.prop (pair (pair nop nop) nop) |> p_elt (fun v -> Type.Map.(elt v) #-- f_fst)) ()
let subst () = f_snd #@ (Reso.map nop nop |> p_add (F.a1 add_sub)) ()





type refi =
  | Var of Type.v
  | Ref of refine


let lo = true
let up = false
let (!) = not


let upd_scp scp v = map_fst (Type.Map.edit (map_snd (map_fst (min scp))) v)
let upd_scp scp v = id

let add_ref scp bnd v = function
  | R_top   -> bounds #.. v #.. bnd #-- b_top #+ true
  | R_siz s -> bounds #.. v #.. bnd #-- b_siz #+ (scp,s)
  | R_idx s -> debug 4 "Add index ! %s" (snd Printer.e v); bounds #.. v #.. bnd #-- b_idx #+ (scp,s)
let add_ref scp b v r = add_ref scp b v r <|> upd_scp scp v

let add scp u v =
  id
  <|> bounds #.. v #.. lo #-- b_var #+ u scp
  <|> bounds #.. u #.. up #-- b_var #+ v scp
let add scp u v = add scp u v <|> upd_scp scp v <|> upd_scp scp u


open Base
open Monad.Intro (M)


let show var (bnds,_) =
  let lo = f_id #@ (Std.pair nop nop) #.. lo #! bnds in
  let up = f_id #@ (Std.pair nop nop) #.. up #! bnds in
  let vars = ~!Printer.vars in
  debug 4 "    %a%a%a%s<: %s <:%a%a%a%s"
    (Type.Map.pp "" (fun p (v,i) -> Format.fprintf p "%s:%d " (snd vars v) i)) lo.b_var
    (List.pp "" (fun p -> snd <|> Format.fprintf p "[%a] " (Printer.eta vars))) lo.b_idx
    (List.pp "" (fun p -> snd <|> Format.fprintf p "<%a> " (Printer.eta vars))) lo.b_siz
    (if lo.b_top then "int " else "")
    (snd vars var)
    (List.pp "" (fun p -> snd <|> Format.fprintf p " [%a]" (Printer.eta vars))) up.b_idx
    (List.pp "" (fun p -> snd <|> Format.fprintf p " <%a>" (Printer.eta vars))) up.b_siz
    (*if List.is_empty up.b_idx then "" else " []"*)
    (*if List.is_empty up.b_siz then "" else " <>"*)
    (Type.Map.pp "" (fun p (v,i) -> Format.fprintf p " %s:%d" (snd vars v) i)) up.b_var
    (if up.b_top then " int" else "")


let add_type_subst info sub scp =
  Type.Map.fold (fun var typ (bnd, sub) ->
      debug 3 "Generate ref vars for %s %a" (snd Printer.e var) (Printer.tau Printer.e) typ;
      let rec map bnd = function
        | T_var _ as t -> bnd, t

        | T_val (T_int _, []) ->
           debug 3 "New type var";
           let v = Type.Var.create () in
           Type.Map.add v ((empty,empty),(Type.Map.get scp var, info var)) bnd, T_var v

        | T_val (a, s) ->
           let bnd, s = NSeq.map_fold map bnd s in
           bnd, T_val (a, s)
      in
      let bnd, typ = map bnd typ in
      (bnd, sub)
      |> subst #-- f_snd #+ var typ
    ) sub

let empty = Type.Map.empty, Ins.empty

let build =
  let* info,sub,scp = read_state (fun s -> snd s.s_loc, snd s.f_sub, snd s.f_scp) in
  let r = add_type_subst (Type.Map.get info) sub scp empty in
  let* () = edit_state f_sub #= (snd r) in
  return r



let add_size_subst sub =
  let sub_s s = Subst.eta (sub,Type.Map.empty) s in
  let sizes l = List.fold (map_snd sub_s)#.insert_size l [] in
  let bound = b_siz #> sizes <|> b_idx #> sizes in
  id
  <|> bounds #> (Type.Map.map (map_fst (Pair.map bound)))
  <|> subst #+ (sub, Type.Map.empty)


let generate scp info (t1,t2) dsc = match t1,t2 with
  | T_var v1, T_var v2 -> add scp v1 v2 dsc |> return
  | T_var v, T_val (T_int r, []) -> add_ref scp up v r dsc |> return
  | T_val (T_int r, []), T_var v -> add_ref scp lo v r dsc |> return
  | T_val (T_int r1, []), T_val (T_int r2, []) ->
     begin match r1,r2 with
     | _, R_top -> return dsc
     | R_idx _, R_idx _ -> return dsc
     | R_siz _, R_siz _ -> return dsc
     | _ -> let* () = error (type_mismatch info t1 t2) in return dsc
     end
  | _ -> failwith "Unexpected constraint"

let generate scp leq info (t1,t2) =
  if leq then generate scp info (t1,t2) else
    generate scp info (t1,t2) <:> generate scp info (t2,t1)

(** Extract equivalence classes (disjoint) **)
let cycles dsc =
  let rec insert s = function
    |   []   -> if Type.Set.is_empty s then [] else s :: []
    | h :: t -> if Type.Set.(inter h s |> is_empty) then h :: insert s t
                else insert Type.Set.(union s h) t
  in
  Type.Map.fold (fun _ ((a,b),_) ->
      insert Type.Map.(Type.Set.inter (keys a.b_var) (keys b.b_var))
    ) dsc []

let cycle_sub cycles =
  List.fold (fun s ->
      let vid = Type.Set.choose s in
      Type.Set.fold (fun v -> Type.Map.add v (Type.unit vid)) (Type.Set.remove vid s)
    ) cycles Type.Map.empty

let ref_sub sub dsc =
  dsc
  |> bounds #> Type.Map.(fold remove#.cst sub)
  |> bounds #> Type.Map.(map (map_fst (Pair.map b_var#>(Type.Map.filter (fun i _ -> mem i sub |> not)))))
  |> subst #+ (Size.Map.empty, sub)


let add_ref_sub var typ dsc =
  let lob = bounds #.. var #.. lo #! dsc in
  let upb = bounds #.. var #.. up #! dsc in
  let ref = match typ with
    | T_val (T_int r, []) -> r
    | _ -> assert false
  in
  dsc
  |> bounds #- var
  |> Type.Map.fold (fun v scp -> bounds #.. v #.. up #-- b_var #- var <|> add_ref scp up v ref) lob.b_var
  |> Type.Map.fold (fun v scp -> bounds #.. v #.. lo #-- b_var #- var <|> add_ref scp lo v ref) upb.b_var
  |> subst #+ (Size.Map.empty, Type.Map.singleton var typ)


let solve (dsc: _ dsc) =

  debug 4 "DESC content";
  TMap.iter show (fst dsc);

  let l_siz = f_fst in
  let l_idx = f_snd in


  let rec add lbl lvl siz var l sub =
    let p,(lv,_) = Type.Map.get sub var in
    let lvl = min l lvl in

    let vars = ~!Printer.vars in
    debug 3  "Add %s %d: %a (prev:%d)" (snd vars var) lvl (Printer.eta vars) siz lbl#--f_fst#!p;

    if lbl#--f_fst#!p >= lvl
    then sub
         |> Type.Map.edit f_fst#@nop#--lbl#--f_fst #> (min lbl#--f_fst#!p) var
    else sub
         |> Type.Map.edit f_fst#@nop#--lbl #= (lvl,siz) var
         |> Type.Map.fold (add lbl lvl siz) bounds#..var#..lo#--b_var#!dsc
  in

  let sub =
    Type.Map.map (map_fst (cst ((-1,Size.undef), (-1,Size.undef)))) (fst dsc)
    |> Type.Map.fold (fun v (d,(i,_)) ->
           id
           <|> List.fold (fun (l,s) -> add l_siz i s v l) f_id#@(Std.pair nop nop)#..up#--b_siz#!d
           <|> List.fold (fun (l,s) -> add l_idx i s v l) f_id#@(Std.pair nop nop)#..up#--b_idx#!d
         ) (fst dsc)
  in
  debug 3 "Add finished";
  (*    let dsc = map_fst (Type.Map.mapi (fun v -> map_fst (Pair.map b_var#-v))) dsc in*)

  let solve var ((siz,idx),(_,info)) sub =
    let* sub in
    let* a_subs = read_param ~:a_sub in
    let subst n = List.fold_right Subst.eta (List.tl#*n a_subs) in


    let add to_type scp siz =
      let vars = ~!Printer.vars in
      debug 3 "Subst %s(%d): %a" (snd vars var) scp (Printer.eta vars) (subst scp siz);

      return (Type.Map.add' var (subst scp siz |> to_type) sub)
    in

    match siz,idx with
    | (-1,_),(-1,_) -> return sub
    | (-1,_),( n,s) -> add index n s
    | ( n,s),(-1,_) -> add size  n s
    | ( _,s), (_,i) -> let* () = error (type_mismatch info (size s) (index i)) in
                       return sub
  in

  let* sub = Type.Map.fold solve sub (return Type.Map.empty) in
  return (ref_sub sub dsc)



let propagate dsc =
  debug 4 "DESC content";
  TMap.iter show (fst dsc);
  let propag var (bnds,_) def =
    debug 3 "Propag %s ?" (snd Printer.e var);
    let lo = f_id #@ (Std.pair nop nop) #.. lo #! bnds in
    if Type.Map.is_empty lo.b_var
                         (*Type.Set.subset (Type.Map.keys lo.b_var) (Type.Map.keys def |> Type.Set.add var)*)
    then match lo.b_siz, lo.b_idx with
         | [s],[] -> debug 4 "Propagate Size !"; Type.Map.add var (snd#.size s) def
         | [],[s] -> debug 4 "Propagate Index !"; Type.Map.add var (snd#.index s) def
         | s,i ->
            let vars = Printer.vars () in
            debug 4 "Sizes: <%a>; Indexes: <%a>"
              (List.pp ", " (Printer.eta vars)) (List.map snd s)
              (List.pp ", " (Printer.eta vars)) (List.map snd i);
            def
    else def
  in
  let rec fix dsc =
    let def = Type.Map.fold propag (fst dsc) Type.Map.empty in
    if Type.Map.is_empty def then dsc else
      dsc
      |> Type.Map.fold add_ref_sub def
      |> fix
  in let dsc = fix dsc in
     debug 4 "DESC content";
     TMap.iter show (fst dsc);
     dsc


let extract (dsc,sub) =
  edit_state f_sub #= (add_sub (Size.Map.empty, Type.Map.map (cst int) dsc) sub)
