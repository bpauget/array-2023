open Std
open Base
open Reso

open Monad.Intro (M)

let solve _ _ info (t1,t2) () =
  debug 3 "Constraint %a <: %a"
    (Printer.tau Printer.e) t1
    (Printer.tau Printer.e) t2;

  (* 'a = 'b *)
  let solve_var v1 v2 =
    scp f_snd v1 <.> scp f_snd v2 >>= function
    | Free s1, Free s2 -> let* () = add_type_sub info v1 (Type.unit v2) in set_scp f_snd v2 s1
    | Abst _, Abst _ -> error (type_mismatch info (Type.unit v1) (Type.unit v2))
    | Free sf, Abst sa -> if sf < sa then error (type_escape info v2) else add_type_sub info v1 (Type.unit v2)
    | Abst sa, Free sf -> if sf < sa then error (type_escape info v1) else add_type_sub info v2 (Type.unit v1)
  in

  (* 'a = t *)
  let solve_sub var typ =
    let* pr,st = current in
    if Type.Map.mem var f_scp #-- f_snd #! st |> not then
      error (type_mismatch info t1 t2)
    else
      try let scp = f_scp #-- f_snd #.. var #! st in
          let move_out lbl escape v var res =
            let* err = res in
            try let a_s = a_scp #-- lbl #.. var #! pr in
                if a_s <= scp then return err
                else let* () = error (escape info var) in
                     return false
            with _ -> let () = debug 2 " scope %s %d" (v var) scp in
                      let* () = set_scp lbl var scp in
                      return err
          in
          let s_vars, t_vars = Utils.Vars.tau typ in
          if Type.Set.mem var t_vars then error (cyclic_type info var typ) else
            let* err =
              return true
              (*              |> Size.Set.fold (move_out f_fst size_escape (fst ~!CPrinter.vars)) s_vars*)
              |> Type.Set.fold (move_out f_snd type_escape (snd ~!Printer.vars)) t_vars
            in
            if err then add_type_sub info var typ else return ()
      with _ -> error (type_escape info var)
  in

  (* Inspect unsolved constraint *)
  match t1,t2 with
  | T_var v1, T_var v2                   -> solve_var v1 v2
  | T_var v, t | t, T_var v              -> solve_sub v t
  | T_val (T_int _,_), T_val (T_int _,_) -> return ()
  | t1, t2                               -> error (type_mismatch info t1 t2)
