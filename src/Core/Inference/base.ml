
open Std
(*open Common*)

type ('f,'s) p = 'f * 's

type free_vars = ( location Size.Map.t
                 , location Type.Map.t ) p

type quan_vars = ( Size.t option loc Size.Map.t
                 , Type.t option loc Type.Map.t ) p

type cstr  = Type.t pair loc

type info = Type.t pair list loc
type 'a tagged = 'a * info




(** Insert substitution extension [SUB_X] in original substitution [SUB_O] **)
let add_sub sub_x sub_o =
  sub_o
  |> map_fst Size.Map.(map (Subst.eta sub_x) <|> union' (fst sub_x))
  |> map_snd Type.Map.(map (Subst.tau sub_x) <|> union' (snd sub_x))





(** Build the subtitution to apply for each level **)
let quant_subst quant =
  List.fold_right (fun (q_sizes,q_types) (sub, subs) ->
      add_sub ( Size.Map.filter_map (cst fst) q_sizes
              , Type.Map.filter_map (cst fst) q_types ) sub,
      sub :: subs
    ) quant (Ins.empty, List.empty)
  |> snd
