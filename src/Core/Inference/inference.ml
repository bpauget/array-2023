


module STypes = Types
module SRefinements = Refinements
module SSizes = Sizes

open Std
open Ast
open Utils




let u_DEBUG = 4
let debug n = (if n < u_DEBUG then Format.fprintf else Format.ifprintf) Format.std_formatter

let _,s = SVar.ids ()
let _,t = TVar.ids ()
let pv = (s,t)
let p_vars = if u_DEBUG = 0 then p_vars else fun () -> t,s

let cstr_map f = map_fst (Pair.map f)


type u_vars = location SMap.t * location TMap.t
type u_subs = eta SMap.t * tau TMap.t
type u_poly = gen PMap.t * ins PMap.t
type u_cstr = tau pair loc list pair



type unif =
  { u_vars : u_vars
  ; u_subs : u_subs
  ; u_poly : u_poly
  ; u_cstr : u_cstr
  }

let u_empty =
  { u_vars = (SMap.empty,TMap.empty)
  ; u_subs = (SMap.empty,TMap.empty)
  ; u_poly = (PMap.empty,PMap.empty)
  ; u_cstr = (List.empty,List.empty) }

let join u1 u2 =
  let s_union a b = SMap.union (fun _ _ -> assert false) a b in
  let t_union a b = TMap.union (fun _ _ -> assert false) a b in
  let p_union a b = PMap.union (fun _ _ -> assert false) a b in
  let (!) fst snd (a,b) (c,d) = fst a c, snd b d in
  { u_vars = ! s_union t_union u1.u_vars u2.u_vars
  ; u_subs = ! s_union t_union u1.u_subs u2.u_subs
  ; u_poly = ! p_union p_union u1.u_poly u2.u_poly
  ; u_cstr = List.(! append append u1.u_cstr u2.u_cstr) }

(* Functional fields *)
let u_vars () = F.rw (fun e -> e.u_vars) (fun m e -> { e with u_vars = m }) (pair (SMap.prop' nop) (TMap.prop' nop))
let u_subs () = F.rw (fun e -> e.u_subs) (fun m e -> { e with u_subs = m }) (pair (SMap.prop' nop) (TMap.prop' nop))
let u_poly () = F.rw (fun e -> e.u_poly) (fun m e -> { e with u_poly = m }) (pair (PMap.prop' nop) (PMap.prop' nop))
let u_cstr () = F.rw (fun e -> e.u_cstr) (fun l e -> { e with u_cstr = l }) (pair (List.prop nop) (List.prop nop))



let p_unif v unif =
  let open Printer in
  let open Format in
  let vars = v in
  printf "@.[SVars]";
  SMap.iter (fun v _ -> printf " %s" (fst vars v)) (fst unif.u_vars);
  printf "@.[TVars]";
  TMap.iter (fun v _ -> printf " %s" (snd vars v)) (snd unif.u_vars);
  printf "@.[SSubs]";
  SMap.iter (fun v s -> printf "@.  | %s => %a" (fst vars v) (eta vars) s) (fst unif.u_subs);
  printf "@.[TSubs]";
  TMap.iter (fun v t -> printf "@.  | %s => %a" (snd vars v) (tau vars) t) (snd unif.u_subs);
  printf "@.[SubTy]";
  List.iter fst#.(fun (t1,t2) -> printf "@.  | @[<hov>%a@;<: %a@]" (tau vars) t1 (tau vars) t2) (fst unif.u_cstr);
  printf "@.[Coerc]";
  List.iter fst#.(fun (t1,t2) -> printf "@.  | @[<hov>%a@;:> %a@]" (tau vars) t1 (tau vars) t2) (snd unif.u_cstr);
  printf "@.";
  unif



let subst_eta (s_sub,t_sub) = MapT.eta t_sub <|> MapS.eta s_sub
let subst_tau (s_sub,t_sub) = MapT.tau t_sub <|> MapS.tau s_sub



module Inf = Solve.Make (STypes) (SRefinements) (SSizes)

let distrib ?(coerce=false) fn =
  let rec distr leq (t1,t2) =
    let equ = false in
    let leq = leq && true in
    if t1 = t2 then id else
    match t1,t2 with (*get_sub Subst.tau t1 <.> get_sub Subst.tau t2 >>= function*)

    | T_val (c1,a1), T_val (c2,a2) as p ->
       let v1 = variance c1 in
       let v2 = variance c2 in
       let open NSeq in
       begin match equal (cst (cst true)) v1 v2 with
       | Eq when c1 = c2 ->
                      NSeq.fold (function
                          | Cov,(t1,t2) -> distr leq (t1,t2)
                          | Equ,(t1,t2) -> distr equ (t1,t2)
                          | Con,(t1,t2) -> distr leq (t2,t1)
                        ) (combine v1 (combine a1 a2))
                    | _ -> match c1,c2 with
                           | T_int _, T_int R_top when leq -> id
                           | T_int R_siz s1, T_int R_siz s2 when Size.equal s1 s2 || coerce -> id
                           | T_int R_idx s1, T_int R_idx s2 when Size.equal s1 s2 || coerce -> id
                           | _ -> fn leq (*from,loc*) p
       end

    | t1,t2 -> fn leq (*from,loc*) (t1,t2)
  in
  function
  | T_val (T_int _, []), T_val (T_int _, []) when coerce -> id
  | p -> distr true p


let solve env unif =
(*  let unif = p_unif ~!p_vars unif in*)
  let a_vars,subst = Inf.solve (cst None) [{ quant = env
                                           ; fvars = unif.u_vars
                                           ; subty = fst unif.u_cstr
                                           ; coerc = snd unif.u_cstr }] in

  let s_sub =
    let rec s_sub v s m =
      if SMap.mem v m then m else
        let m = SSet.fold (fun v -> Option.fold (s_sub v) (SMap.find_opt v (fst subst)))
                  (Vars.eta_sizes s) m in
        SMap.add v (MapS.eta m s) m
    in SMap.fold s_sub (fst subst) SMap.empty
  in
  let t_sub =
    let rec t_sub v t m =
      if TMap.mem v m then m else
        let m = TSet.fold (fun v -> Option.fold (t_sub v) (TMap.find_opt v (snd subst)))
                  (Vars.tau_types t) m in
        TMap.add v (MapT.tau m t |> MapS.tau s_sub) m
    in TMap.fold t_sub (snd subst) TMap.empty
  in
  let subst = s_sub, t_sub in



  unif
  |> u_subs #-- f_fst #> SMap.(map (subst_eta subst) <|> union' (fst subst))
  |> u_subs #-- f_snd #> TMap.(map (subst_tau subst) <|> union' (snd subst))
  |> u_vars #-- f_fst #> (SSet.fold SMap.remove (SMap.keys (fst subst)))
  |> u_vars #-- f_snd #> (TSet.fold TMap.remove (TMap.keys (snd subst)))
  |> u_cstr #-- f_fst #> List.(map (cstr_map (subst_tau subst)))
  |> u_cstr #-- f_snd #> List.(map (cstr_map (subst_tau subst)))



let bind_vars unif =
  let add _ (t1,t2) =
    id
    <|> Gen.union (Utils.Vars.tau t1)
    <|> Gen.union (Utils.Vars.tau t2)
  in
  Gen.empty
  |> List.fold (fst <|> distrib ~coerce:false add ) (fst unif.u_cstr)
  |> List.fold (fst <|> distrib ~coerce:true  add ) (snd unif.u_cstr)


let free_vars bind unif =
  SMap.filter (fun v _ -> SSet.mem v (fst bind) |> not) (fst unif.u_vars),
  TMap.filter (fun v _ -> TSet.mem v (snd bind) |> not) (snd unif.u_vars)









type scheme = (Size.v,Type.v) param list * tau

type env =
  { e_sizes : SSet.t
  ; e_types : TSet.t
  ; e_decls : (scheme * location option) IMap.t }

let e_decls () = F.rw (fun e -> e.e_decls) (fun m e -> { e with e_decls = m }) (imap' nop)
let e_sizes () = F.rw (fun e -> e.e_sizes) (fun s e -> { e with e_sizes = s }) SSet.prop
let e_types () = F.rw (fun e -> e.e_types) (fun s e -> { e with e_types = s }) TSet.prop

include StateMonad.Intro (StateMonad.Make (struct type t = unif end))


let sub_type loc t2 t1 = effect u_cstr #-- f_fst #+ ((t1,t2),loc)
let coercion loc t2 t1 = effect u_cstr #-- f_snd #+ ((t1,t2),loc)


let solve env = effect (solve env)
let subst t =
  let+ subst = read ~:u_subs in
  subst_tau subst t


let new_svar loc =
  let v = SVar.create () in
  let+ () = effect (u_vars #-- f_fst #+ v loc) in
  v

let new_tvar loc =
  let v = TVar.create () in
  let+ () = effect (u_vars #-- f_snd #+ v loc) in
  v


let new_eta loc = let+ v = new_svar loc in Size.unit v
let new_tau loc = let+ v = new_tvar loc in Type.unit v


let register env loc svars tvars =
  id
  <|> u_vars #-- f_fst #> (SSet.fold (fun v -> if SSet.mem v env.e_sizes then id else SMap.add v loc) svars)
  <|> u_vars #-- f_snd #> (TSet.fold (fun v -> if TSet.mem v env.e_types then id else TMap.add v loc) tvars)
  |> effect


let eta env (eta,loc) = register env loc (Vars.eta_sizes eta) (Vars.eta_types eta) >>= fun _ -> return eta
let tau env (tau,loc) = register env loc (Vars.tau_sizes tau) (Vars.tau_types tau) >>= fun _ -> return tau





let remove_free gen =
  u_vars #> (List.fold (param_fold fst#.SMap.remove#.map_fst fst#.TMap.remove#.map_snd) gen)




let decl_var (_,x,_,_) = x

let def_poly lbl loc v l =
  let+ _ = effect (u_poly#--lbl#+ v (l,loc)) in
  l



let loc fn env obj = let+ res = fn env obj in res, snd obj

let param eta tau env = function
  | Size s -> let+ s = eta env s in Size s
  | Type t -> let+ t = tau env t in Type t

let generalize (gen,loc) fn env arg =

  (* Extract variables *)
  let svars,tvars =
    List.fold (param_fold
                 fst#.SSet.add#.map_fst
                 fst#.TSet.add#.map_snd)
      gen (SSet.empty, TSet.empty)
  in

  let* res =
    (env
     |> e_sizes #> (SSet.union svars)
     |> e_types #> (TSet.union tvars)
     |> fn) arg
  in
  let* _ = solve (Size.Set.fold (fun v -> Size.Map.add v (None,dummy_loc)) (SSet.union svars env.e_sizes) Size.Map.empty,
                  Type.Set.fold (fun v -> Type.Map.add v (None,dummy_loc)) (TSet.union tvars env.e_types) Type.Map.empty) in
  let* _ = register env loc svars tvars in
  let* subty,coerc = read ~:u_cstr in
  let+ t = subst res in


  (* Check generalized variables do not escape *)
  let check_escape coerce ((t1,t2),l) =
    let svars, tvars =
      Gen.empty
      |> distrib ~coerce (fun _ (t1,t2) ->
             id
             <|> Gen.union (Vars.tau t1)
             <|> Gen.union (Vars.tau t2)) (t1,t2)
      |> (SSet.inter svars -|- TSet.inter tvars)
    in
    if SSet.is_empty svars && TSet.is_empty tvars then id else begin
        let vars = Printer.vars () in
        p_error l "This expression has type %a@ but was expected of type %a."
          (Printer.tau vars) t1 (Printer.tau vars) t2;
        p_note loc "Size or type variables%a%a would escape their scope"
          (fun p -> SSet.iter (fst vars)#.(Format.fprintf p " %s")) svars
          (fun p -> TSet.iter (snd vars)#.(Format.fprintf p " %s")) tvars;
        cst true
      end
  in



  if false
     |> List.fold (check_escape false) subty
     |> List.fold (check_escape  true) coerc
  then raise Error else t




(* Constraint collecting and solving *)
let rec exp env (exp,loc) =
  let t,unif = init u_empty (exp' env loc exp) in
  let+ () = effect (join unif) in t

and exp' env loc = function

  | E_err -> new_tau loc

  | E_cst C_int _ -> return int
  | E_cst C_bool _ -> return bool

  | E_var (v,i) -> ins env v i

  | E_coe (e,t) ->
     let* t = tau env t in
     let+ _ = exp env e >>= coercion loc t in
     t

  | E_typ (e,t) ->
     let* t = tau env t in
     let+ _ = exp env e >>= sub_type loc t in
     t

  | E_abs ((x,t),e) ->
     let* t = tau env t in
     let+ e = exp (e_decls #+ (fst x) (([],t),None) env) e in
     t => e

  | E_gen (g,e) -> generalize g exp env e

  | E_app (f,a) ->
     let* v = new_tau loc in
     let* a = exp env a in
     let+ _ = exp env f >>= sub_type loc (a => v) in
     v

  | E_cnd (c,(t,f)) ->
     let* a = new_tau loc in
     let+ _ = exp env f >>= sub_type (snd f) a
     and+ _ = exp env t >>= sub_type (snd t) a
     and+ _ = exp env c >>= sub_type (snd c) bool in
     a

  | E_vaS s ->
     let+ s = eta env s in
     size s

  | E_fix (d,i) ->
     let* env = decl env true d in
     ins env (decl_var d) i

  | E_let (d,e) ->
     let* env = decl env false d in
     exp env e

  | E_exS ((s,i),e) ->
     let* _ = exp env i >>= sub_type (snd i) int in
     let* t = generalize ([Size s],snd s) exp env e in
     let+ () = effect u_vars#--f_fst#-(fst s) in

     (* Check variable does not appear in type *)
     if SSet.mem (fst s) (Vars.tau_sizes t) |> not then t else begin
         let vars = Printer.vars () in
         p_error (snd e) "Existentially quantified size variable %s appears@ in expression type %a."
           (fst vars (fst s)) (Printer.tau vars) t;
         p_note (snd s) "Size variable %s declared here." (fst vars (fst s));
         raise Error
       end



(** Environment variable instantiation **)
and ins env (var,l) ins =
  let (gen,t),d = e_decls #.. var #! env in

  (* Forbid explicit instantiation of implicitly polymorphic variables *)
  match d,ins with
  | Some l, P_def (_,l') ->
     p_error l' "Illegal explicit instantiation of implicitly polymorphic variable";
     p_note l "Variable %a declared here" Printer.var (var,l);
     raise Error;
  | _ ->
     let (!) f l _ = let+ r = f l in r,l in
     let+ ins = match ins with
       | P_def i -> mapM (param (loc eta) (loc tau) env) (fst i)
       | P_var v -> mapM (param !new_eta  !new_tau   l ) gen
                    >>= def_poly f_snd l v
     in
     if List.(length gen <> length ins) then
       r_error l "Instanciation arity mismatch: expected %d, encountered %d" (List.length gen) (List.length ins);
     List.fold (function
         | Type v, Type (t,_) -> MapT.tau (TMap.singleton v t)
         | Size v, Size (s,_) -> MapS.tau (SMap.singleton v s)
         | Type _, Size (_,l) -> r_error l "Type expected, size encountered"
         | Size _, Type (_,l) -> r_error l "Size expected, type encountered"
       ) (List.combine gen ins) t


(** Generalization point **)
and decl ?(avars=SSet.empty,TSet.empty) env rec_f (g,x,t,e) =
  let gen,inf,loc = match g with
    | P_def l -> fst l, false, snd l
    | P_var v -> [], not rec_f, snd x
  in
  let (!) = List.map (param_map fst fst) in
  let env' =
    env
    |> (if rec_f then e_decls #+ (fst x) ((!gen,fst t),None) else id)
    |> SSet.fold ((#+) e_sizes) (fst avars)
    |> TSet.fold ((#+) e_types) (snd avars)
  in


  let* e_t = generalize (gen,loc) exp env' (E_typ (e,t), snd e) in


  (* Explicit type variables *)
  let e_vars = (Vars.tau_sizes (fst t),
                Vars.tau_types (fst t)) in

  (* Check generalized variables are not captured *)
  let* subst = read ~:u_subs in
  let svars =
    SSet.empty
    |> SSet.fold (SMap.get_opt (fst subst) <|> Option.fold Vars.eta_sizes#.SSet.union) (fst e_vars)
    |> TSet.fold (TMap.get_opt (snd subst) <|> Option.fold Vars.tau_sizes#.SSet.union) (snd e_vars)
  in
  let tvars =
    TSet.empty
    |> SSet.fold (SMap.get_opt (fst subst) <|> Option.fold Vars.eta_types#.TSet.union) (fst e_vars)
    |> TSet.fold (TMap.get_opt (snd subst) <|> Option.fold Vars.tau_types#.TSet.union) (snd e_vars)
  in
  if List.exists (function
             | Size (v,_) -> SSet.mem v svars
             | Type (v,_) -> TSet.mem v tvars
           ) gen
  then r_error (snd e) "This expression has type %a which is@ less general than type %a. %a"
         (Printer.tau pv) e_t (List.pp " " (fun p -> function
                                    | Size (v,_) -> Format.fprintf p "size %s" (fst pv v)
                                    | Type (v,_) -> Format.fprintf p "type %s" (snd pv v))) gen
         (Printer.tau pv) (fst t);


  let* t =
    if SSet.is_empty (fst avars) && TSet.is_empty (snd avars)
    then return e_t else
      (* Register annonymous variables & generalize them *)
      let* _ = register env (snd x) (fst avars) (snd avars) in
      generalize ([],loc) (fun _ -> return) env e_t
  in


  (* Build and register generalisation list *)
  let* gen =
    let* gen =
      if not inf then return gen else
        (* Extract free unconstrained variables *)
        let* b_vars = read bind_vars in
        let+ g_vars = read (free_vars b_vars) in
        []
        |> TMap.fold (fun v l -> List.cons (Type (v,l))) (snd g_vars)
        |> SMap.fold (fun v l -> List.cons (Size (v,l))) (fst g_vars)
    in match g with
       | P_var v -> def_poly f_fst loc v gen
       | _ -> return gen
  in

  (* Substituted type variables *)
  let t_vars = (Vars.tau_sizes t,
                Vars.tau_types t) in

  (* Check generalized variables appear in type *)
  let check_appear =
    let error kind vars loc var =
      p_error loc "%s variable %s does not appear in type %a" kind (vars pv var) (Printer.tau pv) t;
      cst true
    in function
    | Size (v,l) -> if SSet.mem v (fst t_vars) then id else error "Size" fst l v
    | Type (v,l) -> if TSet.mem v (snd t_vars) then id else error "Type" snd l v
  in

  if List.fold check_appear gen false then raise Error;


  (* Remove generalized variables *)
  let+ _ = effect (remove_free gen) in
  e_decls #+ (fst x) ((!gen,t),if inf then Some loc else None) env





let top_decl env (d,avars) =

  let env,unif =
    init u_empty
      (decl ~avars { e_decls = env
                   ; e_sizes = SSet.empty
                   ; e_types = TSet.empty } false d)
  in

  (* Check that all variables has be generalized *)
  SMap.iter (fun v l -> p_error l "Ungeneralized size variable") (fst unif.u_vars);
  TMap.iter (fun v l -> p_error l "Ungeneralized type variable") (snd unif.u_vars);
(*  if not (SMap.is_empty (fst unif.u_vars) && TMap.is_empty (snd unif.u_vars))
  then raise Error;*)

  (* Check that no constraint remains *)
  let vars = Printer.vars () in

  List.iter (fun ((t1,t2),l) ->
      if distrib (fun _ (t1,t2) _ ->
             p_note l "Incompatible types <%a> and <%a>"
               (Printer.tau vars) t1 (Printer.tau vars) t2;
             true
           ) (t1,t2) false then
      p_error l "Incompatible types <%a> and <%a>"
        (Printer.tau vars) t1 (Printer.tau vars) t2) (fst unif.u_cstr);
  List.iter (fun ((t1,t2),l) ->
      if distrib ~coerce:true (fun _ (t1,t2) _ ->
             p_note l "Incompatible types <%a> and <%a>"
               (Printer.tau vars) t1 (Printer.tau vars) t2;
             true
           ) (t1,t2) false then
        p_error l "Invalid coercion from <%a> to <%a>"
        (Printer.tau vars) t1 (Printer.tau vars) t2) (snd unif.u_cstr);
(*  if fst unif.u_cstr <> [] || snd unif.u_cstr <> [] then raise Error;*)

  env.e_decls,
  (fst unif.u_poly, snd unif.u_poly,
   fst unif.u_subs, snd unif.u_subs)

let init_env =
  IMap.empty
  |> List.fold (fun ((s,x),t) ->
         IMap.add x (t,None)
       ) Builtins.builtins
