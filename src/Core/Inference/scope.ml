open Std

open Base


type t =
  { quant: quan_vars    (* Locally quantified variables *)
  ; fvars: free_vars    (* Scoped free variables *)
  ; subty: cstr list    (* Scoped sub-typing constraints *)
  ; coerc: cstr list }  (* Scoped coercion constraints *)

let empty =
  { quant = Size.Map.empty, Type.Map.empty
  ; fvars = Size.Map.empty, Type.Map.empty
  ; subty = List.empty
  ; coerc = List.empty }

let quant () = F.rw (fun s -> s.quant) (fun p s -> { s with quant = p }) (pair (Size.map nop) (Type.map nop))
let fvars () = F.rw (fun s -> s.fvars) (fun p s -> { s with fvars = p }) (pair (Size.map nop) (Type.map nop))
let subty () = F.rw (fun s -> s.subty) (fun l s -> { s with subty = l }) (list nop)
let coerc () = F.rw (fun s -> s.coerc) (fun l s -> { s with coerc = l }) (list nop)


open Printer
open Format


let dp ?(vars=vars()) { quant; fvars; subty; coerc } =
  p_scope "@[<v>@[<v 2> Scope ------------------------" "@]@; ------------------------ Scope@]"
    [ p_scope "@;@[<v 2>[Locally abstract]" "@]"
        [ p_scope "@;@[<hov 2>[Size]" "@]"
            [ Size.Map.dp "," (fun v (s,_) ->
                  dprintf " %s(@[<hov>%a@])" (fst vars v) (Option.pp (Printer.eta vars)) s) (fst quant) ]
        ; p_scope "@;@[<hov 2>[Type]" "@]"
            [ Type.Map.dp "," (fun v (s,_) ->
                  dprintf " %s(@[<hov>%a@])" (snd vars v) (Option.pp (Printer.tau vars)) s) (snd quant) ]
        ]
    ; p_scope "@;@[<v 2>[Free]" "@]"
        [ p_scope "@;@[<hov 2>[Size]" "@]"
            [ Size.Map.dp "," (fun v _ -> dprintf " %s" (fst vars v)) (fst fvars) ]
        ; p_scope "@;@[<hov 2>[Type]" "@]"
            [ Type.Map.dp "," (fun v _ -> dprintf " %s" (snd vars v)) (snd fvars) ]
        ]
    ; p_scope "@;@[<v 2>[Sub-typing]" "@]"
        [ List.dp "" (fun ((t1,t2),_) -> dprintf "@;@[<hov 2>%a <: %a@]" (Printer.tau vars) t1 (Printer.tau vars) t2) subty ]
    ; p_scope "@;@[<v 2>[Coercion]" "@]"
        [ List.dp "" (fun ((t1,t2),_) -> dprintf "@;@[<hov 2>%a :> %a@]" (Printer.tau vars) t1 (Printer.tau vars) t2) coerc ]
    ]




let rec gather n = function
  | [] -> id
  | { quant; fvars; subty; coerc } :: l ->
     fun ((fs,ft),s,c) ->
     gather (n+1) l
       ( ( Size.Map.(fold (fun v l -> add' v (n)) (fst fvars) fs)
         , Type.Map.(fold (fun v l -> add' v (n)) (snd fvars) ft) )
       , List.(fold (curry id n)#.cons subty s)
       , List.(fold (curry id n)#.cons coerc c) )

let gather l = gather 0 l ( ( Size.Map.empty
                            , Type.Map.empty )
                          , List.empty
                          , List.empty )
