open Std
open Base
open Reso


open Base
open Monad.Intro (M)


(**---- Size unification ----------------**)

(* Try to find a simple substitution over variables [FVARS]
   that cancels polynom [P] *)
let cancel fvars p =
  let vars = Size.vars p in

  let free i p = not (SSet.mem i (Size.vars p)) in

  (* Form 0 = P±i | i not in P *)
  let cancel i = function
    | None when free i Size.(p + unit i) -> Some Size.(i, unit i + p)
    | None when free i Size.(p - unit i) -> Some Size.(i, unit i - p)
    | o -> o
  in

  (* Form 0 = (i-j)*P *)
  let factor i j =
    if SSet.mem i fvars
       && not (SSet.mem j fvars && j <= i)
       && Size.(compose i (unit j) p =@<compare>@ zero)
    then List.cons (i, Size.unit j) else id
  in


  if p =@<Size.compare>@ Size.zero then None
  else match SSet.fold cancel (SSet.inter fvars vars) None with
       | Some s -> Some s
       | None -> match SSet.fold (fun i ->
                           SSet.fold (factor i) vars) vars [] with
                 | [s] -> Some s
                 | _ -> None





type dsc = (int * Size.t pair tagged) list

let empty = List.empty

let extract scp _ info (t1,t2) l=
  let add s1 s2 = (scp,((s1,s2),info))::l |> return in

  (* Inspect unsolved constraint *)
  match t1,t2 with
  | T_val (T_int (R_idx s1), _),
    T_val (T_int (R_idx s2), _) -> add s1 s2
  | T_val (T_int (R_siz s1), _),
    T_val (T_int (R_siz s2), _) -> add s1 s2
  | _ ->
     let* sub = read_state ~:f_sub in
     let _ = type_mismatch info t1 t2 sub false in
     assert false (*let* () = error (type_mismatch info t1 t2) in
                    return l*)


let get_abstr sub a_sub abst free (scp,((s1,s2),info)) =
  let sb = Subst.eta (sub, Type.Map.empty) in
  let s = List.fold_right (fun s -> Subst.eta s <|> sb) (List.tl#*scp a_sub) (sb Size.(s1-s2)) in
  let v = Utils.Vars.eta_sizes s in

  let a,f = Size.Set.partition (fun v -> Size.Map.mem v abst) v in

  let vars = Printer.vars () in
  debug 4 "  Decompose (%d) %a" scp (Printer.eta vars) s;

  let ords = Size.Map.build (cst 1000) a in

  let p = Size.decompose ords s in

  let scps = Size.Map.union' abst free in

  let m =
    List.fold (fun (ord,siz) ->
        let ord = Size.Map.mapi (Size.Map.get ~default:0 ord)#.cst ords in
        let scp = Size.Set.fold (Size.Map.get scps)#.max (Size.vars siz) 0 in
        debug 4 "      ? (%d) %a" scp (Printer.eta vars) siz;
        Size.Map.(union min (filter (Size.Map.get scps)#.((>=) scp)#.cst ord))
      ) p ords
  in

  debug 4 " Factorize order @[<v>%t@]" (Size.Map.dp "@;" (fst vars <|> Format.dprintf "%s: %d") m);

  Size.decompose m s
  |> List.fold (fun (k,s) ->
         debug 4 "    * %a (%t)" (Printer.eta vars) s (Size.Map.dp "," (fst vars <|> Format.dprintf "%s: %d") k);
         List.cons (scp,((s,Size.zero),info))
       )


let solve (dsc: dsc) =
  let dsc_i = List.sort (fun a b -> fst b - fst a) dsc in
  let rec subst poly dsc sub =
    let* pr,st = current in

    let extr (scp,((s1,s2),(_,loc))) =
      let sb = Subst.eta (sub, Type.Map.empty) in
      let s = List.fold_right (fun s -> Subst.eta s <|> sb) (List.tl#*scp pr.a_sub) (sb Size.(s1-s2)) in
      let v = Utils.Vars.eta_sizes s |> SSet.inter (Size.Map.keys (fst st.f_scp)) in

      let a_scp = Size.Set.fold (Size.Map.get ~default:0 (fst pr.a_scp))#.max v 0 in


      let fvars = Size.Set.filter (fun v -> a_scp <= Size.Map.get ~default:0 (fst st.f_scp) v) v in

      cancel fvars s
      |> Option.map (curry id fvars)
    in
    match List.find_map extr dsc with
    | Some (f,(v,s)) ->
       let vars = ~!Printer.vars in
       debug 5 " Size variable %s" (fst vars v);
       let scp = Size.Map.get (fst st.f_scp) v in

       debug 5 " [Size subst] @[<hov>%s => %a@]"
         (fst vars v) (Printer.eta vars) s;

       Size.Set.fold (fun v r -> let* () = set_scp f_fst v scp in r) f
         (subst true dsc (sub |> Size.Map.map (Size.compose v s) |> Size.Map.add v s))

    | None ->


       let vars = ~!Printer.vars in
       debug 4 " Free Var scp @[<v>%t@]" (Size.Map.dp "@;" (fst vars <|> Format.dprintf "%s: %d") (fst st.f_scp));
       debug 4 " Abst Var scp @[<v>%t@]" (Size.Map.dp "@;" (fst vars <|> Format.dprintf "%s: %d") (fst pr.a_scp));


       let abstr_extr =
         List.empty
         |> if poly then List.fold (get_abstr sub pr.a_sub (fst pr.a_scp) (fst st.f_scp)) dsc_i
            else id
       in

       if List.is_empty abstr_extr |> not
       then subst false (List.fold List.cons abstr_extr dsc) sub
       else



         let* pr,st = current in
         List.fold (fun (scp,((s1,s2),info)) r ->
             let sb = Subst.eta (sub, Type.Map.empty) in
             let sub s = List.fold_right (fun s -> Subst.eta s <|> sb) (List.tl#*scp pr.a_sub) (sb s) in

             (*             let sub = sub, Type.Map.empty in
                            let s = Subst.eta sub Size.(s1-s2) in*)
             (* (* TODO: size escape detection *)
                let v = Vars.eta_sizes s in
                let scp = Size.Set.fold (Size.Map.get ~default:0 (fst pr.a_scp))#.max v 0 in
              *)

             if Size.null (sub (Size.(s1-s2))) then r else
               let vars = ~!Printer.vars in
               let () = debug 3 "Incompatible size %a and %a"
                          (Printer.eta vars) (sub s1)
                          (Printer.eta vars) (sub s2) in
               let* () = error (size_mismatch info s1 s2) in r
           ) dsc (return sub)
         |> Size.Map.fold (fun var siz res ->
                let* res in
                let a_scp = Size.Map.get ~default:0 (fst pr.a_scp) in
                let f_scp = Size.Map.get ~fallback:a_scp (fst st.f_scp) in
                let* () = set_scp f_fst var (Size.Set.fold f_scp#.min (Utils.Vars.eta_sizes siz) (f_scp var)) in
                return res
              ) sub
  in

  subst true dsc_i Size.Map.empty
