
open Std



open Base
open Reso



module type Type = sig
  val solve: unit walk
end

module type Refi = sig
  open Monad.Intro (M)

  type 'a dsc


  val build: __ dsc t
  val add_size_subst: Size.t Size.Map.t -> 'a dsc -> 'a dsc
  val generate: __ dsc walk
  val solve: 'a dsc -> xx dsc t
  val propagate: 'a dsc -> 'a dsc
  val extract: xx dsc -> unit t
end

module type Size = sig
  open Monad.Intro (M)

  type dsc
  val empty: dsc
  val extract: dsc walk
  val solve: dsc -> Size.t Size.Map.t t
end





module Make (T:Type) (R:Refi) (S:Size) = struct
  open Base
  open Monad.Intro (M)

  let distr fn (cstr, loc: cstr) acc =

    let rec distr leq from (t1,t2) arg =
      let equ = false in
      let leq = leq && true in
      let from = (t1,t2) :: from in
      get_sub Subst.tau t1 <.> get_sub Subst.tau t2 >>= function

      | T_val (c1,a1), T_val (c2,a2) as p ->
         let v1 = variance c1 in
         let v2 = variance c2 in
         let open NSeq in
         begin match equal (cst (cst true)) v1 v2 with
         | Eq when c1 = c2 ->
            foldM (function
                | Cov,(t1,t2) -> distr leq from (t1,t2)
                | Equ,(t1,t2) -> distr equ from (t1,t2)
                | Con,(t1,t2) -> distr leq from (t2,t1)
              ) (combine v1 (combine a1 a2) |> list) arg
         | _ -> match c1,c2 with
                | T_int _, T_int R_top when leq -> return arg
                | T_int R_siz s1, T_int R_siz s2 when Size.equal s1 s2 -> return arg
                | T_int R_idx s1, T_int R_idx s2 when Size.equal s1 s2 -> return arg
                | _ -> fn leq (from,loc) p arg
         end

      | t1,t2 ->
         if t1 = t2(* || Option.is_some single*) then return arg else
           alias t1 <.> alias t2 >>= function

           | Some t1, Some t2 -> distr leq from (t1,t2) arg
           | Some t1, _ -> distr leq from (t1,t2) arg
           | _, Some t2 -> distr leq from (t1,t2) arg
           | _ -> fn leq (from,loc) (t1,t2) arg
    in
    distr true [] cstr acc


  let distr_ctrs fn =
    foldM (fun (scp,((t1,t2),info)) arg ->
(*        let* sub = a_sub scp in*)

        let* t1 = get_scp Subst.tau scp t1 in
        let* t2 = get_scp Subst.tau scp t2 in
        let vars = Printer.vars () in
        (*_*) debug 4 "  [Cstr %d] @[<hov> %a <: %a@]" scp
          (Printer.tau vars) t1
          (Printer.tau vars) t2;

        distr (fn scp) ((t1,t2),info) arg
      )







  let no_error a = a


  let (-||-) f g (a,b) (c,d) = f a c, g b d


  let solve
        (alias: tau -> tau option) (* Alias definition inlining *)
        (scopes: Scope.t list)
    =

    (*_*) debug 5 "    %s%t" (center ~n:60 ~fill:"<>" " Resolution ")
      (List.dp "" Scope.dp#.(Format.dprintf "@.%t") scopes);

    let abstr = List.map ~:Scope.quant scopes in
    let a_sub = quant_subst abstr in
    let a_scp =
      abstr
      |> List.mapi (fun s -> Size.Map.map (cst s) -|- Type.Map.map (cst s))
      |> List.fold_left (Size.Map.union' -||- Type.Map.union')
           (Size.Map.empty, Type.Map.empty)
    in
    let f_scp, subty, coerc = Scope.gather scopes in



    let res =

      debug 4 "Solving types...";

      let* () = distr_ctrs T.solve subty () in
      let* () = distr_ctrs T.solve coerc () in
      let* () = report in

      debug 4 "Solving refinements...";

      let* r = R.build in

      let* r = distr_ctrs R.generate subty r in
(*      let* r = distr_ctrs R.generate coerc r in*)
      let* r = R.solve r in
      let* () = report in
      let* () = R.extract r in

      debug 4 "Solving sizes...";
      (* TODO: Size variables should be substituted at some points when propag refinements *)

      let vars = Printer.vars () in
(*      let* () = read_state ~:f_sub #. (fun (s,t) ->
          Size.Map.iter (fun i s ->
              debug 3 "   * size %s => %a" (fst vars i) (CPrinter.eta ~vars) s) s;
          Type.Map.iter (fun i t ->
              debug 3 "   * type (%d) %s => %a"
                (Type.Map.get ~default:0 (snd f_scp) i)
                (snd vars i) (Printer.tau vars) t) t
        )
      in
*)
      let* correct_siz = get_scp' Subst.eta in
      let* correct_typ = get_scp' Subst.tau in
      let* () = edit_state f_sub #> (fun (s,t) ->
          Size.Map.mapi (fun i s ->
              debug 3 "   * size (%d) @[<hov>%s => %a@]"
                (Size.Map.get ~default:0 (fst f_scp) i)
                (fst vars i) (Printer.eta vars) s;
              correct_siz (Size.Map.get ~default:0 (fst f_scp) i) s) s,
          Type.Map.mapi (fun i t ->
              debug 3 "   * type (%d) @[<hov>%s => %a@]"
                (Type.Map.get ~default:0 (snd f_scp) i)
                (snd vars i) (Printer.tau vars) t;
              correct_typ (Type.Map.get ~default:0 (snd f_scp) i) t) t
        )
      in

      let* d = distr_ctrs S.extract subty S.empty in
      let* s = S.solve d in
      let* () = edit_state f_sub #> (add_sub (s,Type.Map.empty)) in
      let* () = report in


      debug 4 "Propagating refinements...";

      let* () = R.add_size_subst s r |> R.propagate |> R.extract in


      (* TODO: Size variables should be substituted at some points when propag refinements *)
      let* () = edit_state f_sub #> (fun (s,t) ->
          Size.Map.mapi (fun i -> (Subst.eta (List.nth a_sub (Size.Map.get ~default:0 (fst f_scp) i)))) s,
          Type.Map.mapi (fun i -> (Subst.tau (List.nth a_sub (Type.Map.get ~default:0 (snd f_scp) i)))) t)
      in

      let* correct_siz = get_scp' Subst.eta in
      let* correct_typ = get_scp' Subst.tau in
      let* () = edit_state f_sub #> (fun (s,t) ->
          Size.Map.mapi (fun i s ->
              debug 3 "   * size (%d) @[<hov>%s => %a@]"
                (Size.Map.get ~default:0 (fst f_scp) i)
                (fst vars i) (Printer.eta vars) s;
              correct_siz (Size.Map.get ~default:0 (fst f_scp) i) s) s,
          Type.Map.mapi (fun i t ->
              debug 3 "   * type (%d) @[<hov>%s => %a@]"
                (Type.Map.get ~default:0 (snd f_scp) i)
                (snd vars i) (Printer.tau vars) t;
              correct_typ (Type.Map.get ~default:0 (snd f_scp) i) t) t
        )
      in

      debug 4 "Check remaining...";

      let* b = distr_ctrs (fun _ _ (_,loc) _ _ ->
                    p_note loc "Remaining constraint"; return true
                 ) subty false
      in
      if b then assert false else return ()
    in


    let (), { f_sub; f_scp } =
      res { a_sub; a_scp; alias } { f_scp
                                  ; f_sub = empty_map
                                  ; s_loc = empty_map
                                  ; error = [] } in

    (*_*) debug 5 "    %s" (center ~n:60 ~fill:"<>" "");



    ( Size.Map.filter_map (fun v s -> if Size.Map.mem v (fst f_sub) then None else Some (s,None)) (fst f_scp)
    , Type.Map.filter_map (fun v s -> if Type.Map.mem v (snd f_sub) then None else Some (s,None)) (snd f_scp) ),
    f_sub

end
