open Std


open Base


type ('s,'t) map = 's Size.Map.t * 't Type.Map.t
let map a b = pair (Size.map a) (Type.map b)
let empty_map = Size.Map.empty, Type.Map.empty

type param =
  { a_scp: (int   , int   ) map
  ; a_sub: (Size.t, Type.t) map list
  ; alias: Type.t -> Type.t option }

type state =
  { f_scp: (int   , int   ) map
  ; f_sub: (Size.t, Type.t) map
  ; s_loc: (info  , info  ) map
  ; error: (Ins.t -> bool -> bool) list }


let s_err () = F.rw (fun s -> s.error) (fun l s -> { s with error = l }) (list nop)
let f_scp () = F.rw (fun s -> s.f_scp) (fun p s -> { s with f_scp = p }) (map nop nop)
let f_sub () = F.rw (fun s -> s.f_sub) (fun p s -> { s with f_sub = p }) (map nop nop |> p_add (F.a1 add_sub))
let s_loc () = F.rw (fun s -> s.s_loc) (fun p s -> { s with s_loc = p }) (map nop nop)
let a_scp () = F.ro (fun p -> p.a_scp) (map nop nop)
let a_sub () = F.ro (fun p -> p.a_sub) (list (map nop nop))


module M = struct
  type 'a t = param -> state -> 'a * state
  let return v p s = v, s
  let bind a f p s =
    let a, s = a p s in
    f a p s
end


let read_param f p s = f p, s
let read_state f p s = f s, s
let edit_state f p s = (), f s

open Monad.Intro (M)

type 'a walk = int -> bool -> info -> Type.t pair -> 'a -> 'a t


let alias t = read_param (fun p -> p.alias t)

let get_sub subst elt = read_state (fun s -> subst s.f_sub elt)


let get_scp' subst =
  let* f_sub = read_state ~:f_sub in
  let* a_sub = read_param ~:a_sub in
  return (
      fun scp ->
      List.fold_right (fun s -> subst s <|> subst f_sub) (List.tl#*scp a_sub)
    )

let get_scp subst scp elt =
  let* sub = read_param ~:a_sub in
  return elt
  |> List.fold_right (fun s e -> e >>- subst s >>= get_sub subst) (List.tl#*scp sub)



let (<.>) a b = (and*) a b
let (<:>) f g a = return a >>= f >>= g

let current = read_param id <.> read_state id

type ('a,'b) var = Free of 'a | Abst of 'b

let scp lbl var =
  let* pr,st = current in
  return (
      try       Abst a_scp#--lbl#..var#!pr
      with _ -> Free f_scp#--lbl#..var#!st
    )

let set_scp lbl var scp = edit_state f_scp #-- lbl #.. var #> (min scp)


let add_type_sub info var typ =

  let vars = ~!Printer.vars in
  debug 8 "  [Subst] @[<hov>%s => %a@]" (snd vars var) (Printer.tau vars) typ;

  edit_state (
      id
      <|> f_sub #+ (Size.Map.empty, Type.Map.singleton var typ)
      <|> s_loc #-- f_snd #+ var info
    )


(** ------------ Errors ------------ **)

let error report = edit_state (s_err #+ report)

let report =
  let* sub = read_state ~:f_sub in
  edit_state s_err #> (fun l -> if List.fold ((|>) sub) l false then raise Error else [])


let incompatible_type ?(vars=Printer.vars()) ((en,ex),loc) =
  p_error loc "This expression has type %a@ but an expression of type %a was expected"
    (Printer.tau vars) en (Printer.tau vars) ex


let incompatible_type vars (info,loc) sub =
  incompatible_type ~vars List.(hd (rev info) |> Pair.map (Subst.tau sub), loc)


let type_mismatch info t1 t2 sub =
  let vars = Printer.vars () in
  incompatible_type vars info sub;
  p_note (snd info) "Type %a is not compatible with type %a"
    (Printer.tau vars) (Subst.tau sub t1)
    (Printer.tau vars) (Subst.tau sub t2);
  cst true

let size_mismatch info s1 s2 sub =
  let vars = Printer.vars () in
  incompatible_type vars info sub;
  p_note (snd info) "Size %a is not compatible with size %a"
    (Printer.eta vars) (Subst.eta sub s1)
    (Printer.eta vars) (Subst.eta sub s2);
  cst true

let cyclic_type info v t sub =
  match Subst.tau sub (Type.unit v) |> Type.to_unit with
  | None -> type_mismatch info (Type.unit v) t sub
  | Some v ->
     let vars = Printer.vars () in
     incompatible_type vars info sub;
     p_note (snd info) "Type variable %s occurs in %a" (snd vars v) (Printer.tau vars) (Subst.tau sub t);
     cst true

(*
  let refi_mismatch info r1 r2 sub =
    let vars = Printer.vars () in
    incompatible_type vars info sub;
    p_note (snd info) "Type %a is not compatible with type %a"
      (Printer.tau ~vars 0) (Subst.tau sub t1)
      (Printer.tau ~vars 0) (Subst.tau sub t2);
    cst true
 *)

let type_escape info v sub =
  let vars = Printer.vars () in
  incompatible_type vars info sub;
  p_note (snd info)  "Type variable %s would escape its scope" (snd vars v);
  cst true

let size_escape info v sub =
  let vars = Printer.vars () in
  incompatible_type vars info sub;
  p_note (snd info)  "Size variable %s would escape its scope" (fst vars v);
  cst true

