(** -------- Abstract Syntax Tree of the Core language -------- **)


(**
   Conventions:
   - To make extensions of the language easier, most
     pattern matchings are exhaustive (causing a warning
     if type is extended). The remaining generic cases
     are marked with (*! DFLT typ !*) comments (where
     typ is the names of the type) for easy finding.
 **)

open Std



(* Variables *)
type var = ident loc
type 'a typed = 'a * tau loc

(* Expressions *)
type exp = exp'  loc and exp' =
  | E_var of var * ins pvar
  | E_cst of cst
  | E_coe of exp typed
  | E_typ of exp typed
  | E_app of exp * exp
  | E_abs of var typed * exp
  | E_gen of gen * exp
  | E_fix of decl * ins pvar
  | E_let of decl * exp
  | E_exS of (Size.v loc * exp) * exp
  | E_vaS of Size.t loc
  | E_cnd of exp * exp pair
  | E_err

and ins = (Size.t loc, Type.t loc) param list loc
and gen = (Size.v loc, Type.v loc) param list loc

(* Inner declarations *)
and decl = gen pvar * var * tau loc * exp

(* Top level declarations contains:
   - the inner declaration
   - a test directive (FAIL or SUCC comment)
 *)
type top_decl = decl * (SSet.t * TSet.t)

type implementation = top_decl tested list

let p_vars () = snd (TVar.ids ()), snd (SVar.ids ())

