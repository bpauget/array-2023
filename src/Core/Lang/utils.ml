

open Std
open Ast



(** Default structure mapping **)
module AstMap = struct
  let tau fn = function
    | T_val (c,a) -> T_val (c, NSeq.map fn a)
    | T_var v -> T_var v

  let decl fn (g,x,t,e) = (g,x,t,fn e)

  let exp fn = function
    | E_coe (e,t) -> E_coe (fn e, t)
    | E_typ (e,t) -> E_typ (fn e, t)
    | E_app (f,e) -> E_app (fn f, fn e)
    | E_abs (x,e) -> E_abs (x, fn e)
    | E_gen (g,e) -> E_gen (g, fn e)
    | E_fix (d,i) -> E_fix (decl fn d, i)
    | E_let (d,e) -> E_let (decl fn d, fn e)
    | E_exS (s,e) -> E_exS (map_snd fn s, fn e)
    | E_cnd (c,b) -> E_cnd (fn c, Pair.map fn b)
    | E_var _ | E_cst _ | E_vaS _ | E_err as e -> e
end

let subst_decl pat exp subst (g,x,p,e) = g, x, pat subst p, exp subst e
let subst_param tau eta subst = function
  | Size s -> Size (eta subst s)
  | Type t -> Type (tau subst t)

let s_subst subst key = SMap.assoc Size.unit key subst
let t_subst subst key = TMap.assoc Type.unit key subst

let loc fn arg = map_fst (fn arg)

(** Size variables mapping **)
(** Case of size abstraction:
      Viewed as a local abstract type:
      if variable substituted, abstraction is dumped
    Case of size quantification in patterns:
      Must be fresh (unmapped) variable
 **)
module MapS = struct
  module Fn = struct
    let of_subst subst = SMap.fold Size.compose subst

    let eta fn : eta -> eta = fn

    let rec tau fn = function
      | T_val (T_int (R_siz s), []) -> T_val (T_int (R_siz (fn s)), [])
      | T_val (T_int (R_idx s), []) -> T_val (T_int (R_idx (fn s)), [])
      | t -> AstMap.tau (tau fn) t

    let rec exp fn =
      map_fst (function
          | E_var (v,i) -> E_var (v, ins fn i)
          | E_vaS s -> E_vaS (loc eta fn s)
          | E_coe (e,t) -> E_coe (exp fn e, loc tau fn t)
          | E_typ (e,t) -> E_typ (exp fn e, loc tau fn t)
          | E_abs (v,e) -> E_abs (map_snd (loc tau fn) v, exp fn e)
          | E_gen (g,e) -> E_gen (g, exp fn e)
          | E_fix (d,i) -> E_fix (subst_decl (loc tau) exp fn d, ins fn i)
          | E_let (d,e) -> E_let (subst_decl (loc tau) exp fn d, exp fn e)
          | E_cst _ | E_app _ | E_exS _ | E_cnd _ | E_err as e
            -> AstMap.exp (exp fn) e)
    and ins fn = function
      | P_var p -> P_var p
      | P_def l -> P_def (map_fst (List.map (subst_param (loc tau) (loc eta) fn)) l)
  end

  let eta = Fn.(of_subst <|> eta)
  let tau = Fn.(of_subst <|> tau)
  let exp = Fn.(of_subst <|> exp)
  let decl d = Fn.(of_subst <|> subst_decl (loc tau) exp) d
end






(** Type variables mapping **)
(** Case of type abstraction:
      Viewed as a local abstract type:
      if variable substituted, abstraction is dumped
    Case of size quantification in patterns:
      Must be fresh (unmapped) variable
 **)
module MapT = struct
  module Fn = struct
    let of_subst subst = t_subst subst

    let eta fn : eta -> eta = id

    let rec tau fn = function
      | T_var v -> fn v
      | t -> AstMap.tau (tau fn) t

    let rec exp fn =
      map_fst (function
          | E_var (v,i) -> E_var (v, ins fn i)
          | E_coe (e,t) -> E_coe (exp fn e, loc tau fn t)
          | E_typ (e,t) -> E_typ (exp fn e, loc tau fn t)
          | E_abs (v,e) -> E_abs (map_snd (loc tau fn) v, exp fn e)
          | E_gen (g,e) -> E_gen (g, exp fn e)
          | E_fix (d,i) -> E_fix (subst_decl (loc tau) exp fn d, ins fn i)
          | E_let (d,e) -> E_let (subst_decl (loc tau) exp fn d, exp fn e)
          | E_cst _ | E_app _ | E_vaS _ | E_exS _ | E_cnd _ | E_err as e
            -> AstMap.exp (exp fn) e)
    and ins fn = function
      | P_var p -> P_var p
      | P_def l -> P_def (map_fst (List.map (subst_param (loc tau) (loc eta) fn)) l)
  end

  let eta = Fn.(of_subst <|> eta)
  let tau = Fn.(of_subst <|> tau)
  let exp = Fn.(of_subst <|> exp)
  let decl d = Fn.(of_subst <|> subst_decl (loc tau) exp) d
end

module MapG = struct
  module Fn = struct
    let of_subst subst v = P_def (PMap.find v subst)

    let decl exp fn = function
      | P_var v,x,t,e -> fn v,x,t, exp fn e
      | v,x,t,e -> v,x,t, exp fn e

    let rec exp fn =
      map_fst (function
          | E_fix (d,i) -> E_fix (decl exp fn d, ins fn i)
          | E_let (d,e) -> E_let (decl exp fn d, exp fn e)
          | E_typ _ | E_coe _ | E_abs _ | E_gen _ | E_var _
            | E_cst _ | E_app _ | E_vaS _ | E_exS _ | E_cnd _ | E_err as e
            -> AstMap.exp (exp fn) e)
    and ins fn = id
  end

  let exp = Fn.(of_subst <|> exp)
  let decl d = Fn.(of_subst <|> decl exp) d
end

module MapI = struct
  module Fn = struct
    let of_subst subst v = P_def (PMap.find v subst)

    let decl exp fn (v,x,t,e) = v,x,t, exp fn e

    let rec exp fn =
      map_fst (function
          | E_var (v,i) -> E_var (v, ins fn i)
          | E_fix (d,i) -> E_fix (decl exp fn d, ins fn i)
          | E_typ _ | E_coe _ | E_abs _ | E_gen _ | E_let _
            | E_cst _ | E_app _ | E_vaS _ | E_exS _ | E_cnd _ | E_err as e
            -> AstMap.exp (exp fn) e)
    and ins fn = function
      | P_var v -> fn v
      | v -> v
  end

  let exp = Fn.(of_subst <|> exp)
  let decl d = Fn.(of_subst <|> decl exp) d
end




(** Type / Size variable extraction from various structures **)
module Vars = struct

  (* Size variables of size *)
  let eta_sizes s = Size.vars s

  (* Size variables of type *)
  let tau_sizes t =
    let rec vars = function
      | T_var _ -> id
      | T_val (c,a) -> (* TODO: quantification... *)
         match c with
         | T_int (R_siz s | R_idx s) -> Size.vars s |> SSet.union
         | _ ->  NSeq.fold vars a
    in vars t SSet.empty


  (* Type variables of size *)
  let eta_types (s:eta) = TSet.empty

  (* Type variables of type *)
  let tau_types t =
    let rec vars = function
      | T_var v -> TSet.add v
      | T_val (_,a) -> NSeq.fold vars a  (* TODO: quantification... *)
    in vars t TSet.empty

  let eta t = eta_sizes t, eta_types t
  let tau t = tau_sizes t, tau_types t
end
