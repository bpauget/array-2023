open Std
open Ast
open Format



type assoc = NA | LtoR | RtoL

let prec_level = function
  | E_err         -> 16, NA
  | E_var _       -> 16, NA
  | E_cst _       -> 16, NA
  | E_vaS _       -> 16, NA
  | E_coe _       -> 0, NA
  | E_typ _       -> 0, NA
  | E_app _       -> 15, LtoR
  | E_cnd _       -> 4, LtoR
  | E_fix _       -> 3,RtoL
  | E_let _       -> 3,RtoL
  | E_exS _       -> 3,RtoL
  | E_abs _       -> 3,RtoL
  | E_gen _       -> 3,RtoL
let atom = 6

(*
let prec_level_t = function
  | T_var _ -> 16, NA
  | T_base _ -> 16, NA
  | T_size _ -> 16, NA
  | T_index _ -> 16, NA
  | T_arrow (T_index _, t) -> 15,RtoL
  | T_arrow _ -> 14, RtoL

let base_type_s = function
  | T_int -> "int"
  | T_bool -> "bool"
*)




let s_sum  = 1, LtoR
let s_prod = 2, LtoR
let s_atom = 3, NA

let t_arrow = 1, RtoL
let t_tuple = 2, NA
let t_array = 3, RtoL
let t_atom  = 4, NA



let bracketed elt_prec elt prec p e =
  let l,a = elt_prec e in
  let lP,rP = match a with
    | LtoR -> l  , 1+l
    | RtoL -> l+1,   l
    | NA   -> l+1, 1+l
  in
  fprintf p (if l < prec then "(@[<hov>%a@])" else "%a") (elt lP rP) e

let eta_prec s =
  match Size.to_unit s with
  | Some _ -> s_atom
  | None -> match Size.to_scalar s with
            | Some n when n >= 0 ->  s_atom
            | _ -> s_sum

let tau_prec = function
  | T_var _ -> t_atom
  | T_val (c,a) ->
     match c,a with
     | _, [] -> t_atom
     | T_fun  , [T_val (T_int (R_idx _), []); _] -> t_array
     | T_arr, _ -> t_array
     | T_tpl _, _ -> t_tuple
     | T_fun  , _ -> t_arrow
     | _          -> t_tuple


let ident p v =fprintf p "%s" (Names.to_string v)
let var p (v,_) = ident p v

(* Size pretty printer *)
let eta vars p = Size.print (fst vars) string_of_int p


let rec tau prec vars =
  bracketed tau_prec (fun lP rP p -> function
    | T_var v -> fprintf p "%s" (snd vars v)
    | T_val (c,a) ->
       match c,a with
       | T_int R_top  , [] -> fprintf p "int"
       | T_int R_siz s, [] -> fprintf p "<%a>" (eta vars) s
       | T_int R_idx s, [] -> fprintf p "[%a]" (eta vars) s
       | T_fun, [T_val (T_int (R_idx s),[]); t] ->
          fprintf p "[%a]%a" (eta vars) s (tau rP vars) t

       | T_fun, [t; u] ->
          fprintf p "%a ->@;%a" (tau lP vars) t (tau rP vars) u

       | T_arr, [s; t] ->
          fprintf p "%a%a" (tau lP vars) s (tau rP vars) t

       | T_tpl _, [] ->
          fprintf p "unit"

       | T_tpl _, [t] ->
          fprintf p "( %a )" (tau 0 vars) t

       | T_tpl _, l ->
          List.pp " *@," (tau lP vars) p (NSeq.list l)

       | T_def (c,[]),l ->
          fprintf p "%s" (Names.to_string c)

       | T_def (c,_),l ->
          fprintf p "%s(%a)" (Names.to_string c) (List.pp ",@;" (tau 13 vars)) (NSeq.list l)
    ) prec




let var p (v,_) = fprintf p "%s" (Names.to_string v)
(*
let eta vars p = Size.print (snd vars) string_of_int p

let rec tau vars prec p t =
  let level,assoc = prec_level_t t in
  let lP,rP = match assoc with
    | LtoR -> level  , 1+level
    | RtoL -> level+1,   level
    | NA   -> level+1, 1+level
  in
  let f p = function
    | T_var v -> fprintf p "%s" (fst vars v)
    | T_base t -> fprintf p "%s" (base_type_s t)
    | T_size s -> fprintf p "<%a>" (eta vars) s
    | T_index s -> fprintf p "[%a]" (eta vars) s
    | T_arrow (T_index s,t) -> fprintf p "[%a]%a" (eta vars) s (tau vars rP) t
    | T_arrow (t,u) -> fprintf p "%a ->@ %a" (tau vars lP) t (tau vars rP) u
  in
  fprintf p (if level < prec  then "(@[<hov>%a@])" else "%a") f t
*)
let fnc vars p = fprintf p "@[<hov>%a@]" (tau 15 vars)
let tau vars p = fprintf p "@[<hov>%a@]" (tau 0 vars)


let pat vars p (abs,t) =
  let p_end o = if o <> "" then fprintf p ". " in
  let p_var k v o =
    if k = o then fprintf p " %s" v
    else (p_end o; fprintf p "%s %s" k v);
    k
  in
  List.fold (param_fold
               fst#.(fst vars)#.(p_var "size")
               fst#.(snd vars)#.(p_var "type")) abs ""
  |> p_end;
  tau vars p (fst t)

(*
let pat vars =
  let e = "" in
  let s = "size" in
  let t = "type" in

  let rec pat c p = function
    | T_mono t -> fprintf p "%s%a" (if c = e then "" else ". ") (tau vars 0) t
    | T_poly (v,sg) ->
       let n,v = match v with
         | Size v -> s,snd vars v
         | Type v -> t,fst vars v
       in
       fprintf p "%s %s%a" (if c = n then "" else if c = e then n else ". "^n) v (pat n) sg
  in
  pat e
*)

let param vars eta tau p = function
  | Size s -> fprintf p "size %a" (eta vars) s
  | Type t -> fprintf p "type %a" (tau vars) t

let poly vars f_eta f_tau p = function
  | P_var _ -> ()
  | P_def l ->  fprintf p " [%a]" (List.pp ", " (param vars
                                                   (fun v p -> fst <|> f_eta <|> eta v p)
                                                   (fun v p -> fst <|> f_tau <|> tau v p))) (fst l)

let rec exp vars prec p e =
  let level,assoc = prec_level (fst e) in
  let lP,rP = match assoc with
    | LtoR -> level  , 1+level
    | RtoL -> level+1,   level
    | NA   -> level+1, 1+level
  in
  let f p = function
    | E_err       -> fprintf p "."
    | E_var (x,i) -> fprintf p "%a%a" var x (ins vars) i
    | E_cst c     -> fprintf p "%s" (cst_s c)
    | E_vaS s -> fprintf p "<%a>" (eta vars) (fst s)
    | E_coe (e,t) -> fprintf p "%a:>@;%a" (exp vars lP) e (tau vars) (fst t)
    | E_typ (e,t) -> fprintf p  "%a:@;%a" (exp vars lP) e (tau vars) (fst t)
    | E_app (f,e) -> fprintf p "%a@;%a" (exp vars lP) f (exp vars rP) e
    | E_abs ((x,t),e) -> fprintf p "fun %a:%a ->@;%a" (var) x (tau vars) (fst t) (exp vars rP) e
    | E_fix (d,i) -> fprintf p "@[<hov>%a@]" (decl (asprintf "fix %a" (ins vars) i) vars) d
    | E_let (d,f) -> fprintf p "@[<v>@[<hov>%a@;in@]@;@]%a" (decl "let" vars) d (exp vars rP) f
    | E_exS ((v,e),f) -> fprintf p "@[<hov>@[<hov 2>let size %s =@;%a@;in@]@;@]%a" (fst vars (fst v))
                             (exp vars 1) e (exp vars rP) f
    | E_cnd (c,(t,f)) -> fprintf p "@[<v>@[<hov 2>case %a@]@;@[<hov 2>then %a@]@;@[<hov 2>else %a@]@]"
                            (exp vars 0) c (exp vars 0) t (exp vars 0) f
    | E_gen (i,e) -> fprintf p "fun%a ->@;%a" (gen vars) (P_def i) (exp vars rP) e
  in
  fprintf p (if level < prec  then "(@[<hov>%a@])" else "%a") f (fst e)

and ins vars = poly vars id id
and gen vars = poly vars Size.unit Type.unit


and decl intro vars p (g,x,t,e) =
  fprintf p "@[<hov 2>%s %a%a: %a =@;%a@]" intro var x (gen vars) g (tau vars) (fst t)
    (exp vars 1) e

let vars () =
  snd (SVar.ids ()), snd (TVar.ids ())


let e = vars () (*
let vars () = e
*)

let top_decl p ((d,_),_) =
  let _,t = TVar.ids () in
  let _,s = SVar.ids () in
  decl "let" (s,t) p d

let implementation p =
  fprintf p "@[<v>%a@]@." (List.pp "@;@;" top_decl)




let p_scope ?(sep:(unit, formatter, unit) format="") beg_ end_ cnt =
  match List.filter (asprintf "%t" <|> String.equal "" <|> not) cnt with
  | [] -> cst ()
  | cnt -> dprintf "%t%t%t" (dprintf beg_) (List.dp sep id cnt) (dprintf end_)
