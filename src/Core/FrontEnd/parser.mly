(** -------- Direct parser of the Core language -------- **)

%{
    open Std
    open Cst

    let (!:) = fst
    let (!) = snd

    let none l = None, l
    let some (o,l) = Some (o,l),l
    let dflt d = function
      | None, l -> d,l
      | Some o, _ -> o

    (* Syntactical sugar for sizes:  a - b = a + (-1)*b *)
    let minus a l b =
      S_add (a,some (S_mul (some (S_int (-1,l),l), b), l <-> !b))

    (* Syntactical sugar for types:  [s]t = [s] -> t *)
    let array s l t =
      T_arrow (some (T_index s,l), t)

    let position p =
      { l_file = p.Lexing.pos_fname;
        l_lbeg = p.pos_lnum;
        l_lend = p.pos_lnum;
        l_cbeg = p.pos_bol;
        l_cend = p.pos_bol; }
%}

(**** Tokens ****)
%token <bool Std.loc> TEST
%token <Std.typ Std.loc> TYP
%token <Std.location> LET FIX FUN IN ARROW EQUAL SIZE TYPE IF THEN ELSE
%token <Std.location> LPAREN RPAREN LBRACK RBRACK LANGLE RANGLE LCURLY RCURLY
%token <Std.location> COLON COERCE COMMA UNDERSCORE DOT
%token <int    Std.loc> INT
%token <bool   Std.loc> BOOL
%token <string Std.loc> IDENT BUILTIN LETTER
%token <Std.location> PLUS STAR MINUS
%token EOF

(**** Precedence ****)
%left PLUS MINUS
%left STAR
%left LPAREN LANGLE INT BOOL IDENT BUILTIN DOT app

%start <Cst.top_decl list> program
%%


(**** Utils ****)
%inline paren (item): l=LPAREN i=item r=RPAREN { i, l <-> r } // ( . )
%inline brack (item): l=LBRACK i=item r=RBRACK { i, l <-> r } // [ . ]
%inline curly (item): l=LCURLY i=item r=RCURLY { i, l <-> r } // { . }
%inline angle (item): l=LANGLE i=item r=RANGLE { i, l <-> r } // < . >

%inline opt (item): // Inlined explicit placeholder
  | l=UNDERSCORE { None  ,  l }
  | i=item       { Some i, !i }

%inline impl (item): // Inlined implicit item
  |              { None  , position $endpos }
  | i=item       { Some i, !i               }

%inline param(eta,tau):
  | l=SIZE s=eta { Size s }
  | l=TYPE t=tau { Type t }

avr: LETTER     { $1            }   // Anonymous variable
var: IDENT      { $1            }   // Normal variable
ivr: opt(IDENT) { $1 |> dflt "" }   // Ignorable variable

cst:
  | i=INT  { C_int  !:i, !i }
  | b=BOOL { C_bool !:b, !b }

(**** Sizes *****)
eta: opt(eta_) { $1 }
eta_:
  | n=INT                { S_int n    , !n        }
  | v=var                { S_var v    , !v        }
  | v=avr                { S_var v    , !v        }
  | a=eta PLUS b=eta     { S_add (a,b), !a <-> !b }
  | a=eta STAR b=eta     { S_mul (a,b), !a <-> !b }
  | a=eta l=MINUS b=eta  { minus a l b, !a <-> !b }
  | e=paren(eta_)        { !: !:e     , !e        }


(**** Types *****)
tau: opt(tau_) { $1 }
fnc: opt(fnc_) { $1 }
tau_:
  | b=TYP               { T_base !:b    , !b }
  | v=var               { T_var v       , !v }
  | v=avr               { T_var v       , !v }
  | s=angle(eta)        { T_size !:s    , !s        }
  | s=brack(eta)        { T_index !:s   , !s        }
  | s=brack(eta) t=tau  { array !:s !s t, !s <-> !t }
  | t=paren(fnc_)       { !: !:t        , !t        }
fnc_:
  | t=tau_             { t }
  | t=tau ARROW u=fnc  { T_arrow (t, u), !t <-> !u }


(**** Expressions ****)
exp:
  | l=DOT                   { E_err        , l         }
  | x=var s=ins             { E_var (x,s)  , !x <-> !s }
  | x=BUILTIN s=ins         { E_var (x,s)  , !x <-> !s }
  | c=cst                   { E_cst !:c    , !c        }
  | s=angle(eta)            { E_vaS !:s    , !s        }
  | f=exp e=exp   %prec app { E_app (f,e)  , !f <-> !e }
  | e=paren(coerce)         { !: !:e       , !e        }
  | e=paren(exp_)           { !: !:e       , !e        }
exp_:
  | e=exp { e }
  | l=FIX s=ins d=decl                        { E_fix (!:d,s)  , l <-> !d }
  | l=LET d=decl IN e=exp_                    { E_let (!:d,e)  , l <-> !e }
  | l=LET SIZE v=ivr EQUAL s=exp_ IN e=exp_   { E_exS ((v,s),e), l <-> !e }
  | l=FUN x=ivr COLON t=tau ARROW e=exp_      { E_abs ((x,t),e), l <-> !e }
  | l=IF c=exp_ THEN t=exp_ ELSE f=exp_       { E_cnd (c,(t,f)), l <-> !f }

ins: impl(brack(separated_list(COMMA,param(eta,tau)))) { $1 }
gen: impl(brack(separated_list(COMMA,param(var,var)))) { $1 }

coerce:
  | e=exp COERCE t=fnc { E_coe (e,t), !e <-> !t }
  | e=exp COLON  t=fnc { E_typ (e,t), !e <-> !t }

decl: x=ivr v=gen COLON t=fnc EQUAL e=exp_ { (v,x,t,e), !x <-> !e }


(**** Value declaration ****)
let_decl: l=LET d=decl   { !:d, l <-> !d }

top_decl: d=let_decl t=TEST? { d,t }


(**** Entry point *****)
program: d=top_decl* EOF { d }
