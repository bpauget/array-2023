(** -------- Concrete Syntax Tree of the Core language -------- **)

open Std

type ident = string
type var = ident loc

type 'a opt = 'a opt' loc
and 'a opt' = 'a loc  option


type eta = eta' opt and eta' =
  | S_var of var
  | S_int of int loc
  | S_add of eta pair
  | S_mul of eta pair

type tau = tau' opt and tau' =
  | T_var   of var
  | T_base  of typ
  | T_size  of eta
  | T_index of eta
  | T_arrow of tau pair

type 'a typed = 'a * tau
type 'a named = var * 'a

type ins = (eta,tau) param list opt
type gen = (var,var) param list opt

type exp = exp' loc and exp' =
  | E_var of var * ins
  | E_cst of cst
  | E_coe of exp typed
  | E_typ of exp typed
  | E_app of exp * exp
  | E_abs of var typed * exp
  | E_fix of decl * ins
  | E_let of decl * exp
  | E_exS of exp named * exp
  | E_vaS of eta
  | E_cnd of exp * exp pair
  | E_err

and decl = gen * var * tau * exp

and top_decl = decl loc tested
