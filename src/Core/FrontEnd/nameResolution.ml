(** -------- Name resolution -------- **)

(** This pass transforms CST into AST by giving unique
    names to variables and introducing fresh names for
    place-holders.
**)

open Std
open Cst
module A = Ast

module S = Set.Make (String)
module M = Map.Make (String)

module AnonymousVars = struct

  let avr f (s,_) =
    if String.length s = 2 && s.[0] = '\'' then f (S.add s)
    else id

  let opt f = fst <|> Option.fold f

  let rec eta s =
    opt fst#.(function
              | S_var v -> avr map_fst v
              | S_int _ -> id
              | S_add p -> Pair.fold eta p
              | S_mul p -> Pair.fold eta p
    ) s

  let rec tau t =
    opt fst#.(function
              | T_var v -> avr map_snd v
              | T_base _ -> id
              | T_size s -> eta s
              | T_index s -> eta s
              | T_arrow p -> Pair.fold tau p
    ) t

  let rec exp e =
    fst#.(function
          | E_var (_,i) -> ins i
          | E_cst _ -> id
          | E_coe (e,t) -> exp e <|> tau t
          | E_typ (e,t) -> exp e <|> tau t
          | E_app (e,f) -> exp e <|> exp f
          | E_abs ((_,t),e) -> tau t <|> exp e
          | E_fix (d,i) -> decl d <|> ins i
          | E_let (d,e) -> decl d <|> exp e
          | E_exS ((_,e),f) -> exp e <|> exp f
          | E_vaS s -> eta s
          | E_cnd (c,b) -> exp c <|> Pair.fold exp b
          | E_err -> id
    ) e

  and ins i = opt fst#.(List.fold (param_fold eta tau)) i

  and decl (_,_,t,e) = tau t <|> exp e

end



type env =
  { expr_vars : (string, Std.ident * int) Hashtbl.t
  ; size_vars : (string, SVar.t    * int) Hashtbl.t
  ; type_vars : (string, TVar.t    * int) Hashtbl.t }

let new_var v = Names.create v
let new_size _ = Size.unit (SVar.create ())
let new_type _ = Type.unit (TVar.create ())

let expr_vars () = F.ro (fun e -> e.expr_vars) nop
let size_vars () = F.ro (fun e -> e.size_vars) nop
let type_vars () = F.ro (fun e -> e.type_vars) nop

let var_k = expr_vars, ""     , Names.create
let siz_k = size_vars, "size ", (fun id -> SVar.create ~id ())
let typ_k = type_vars, "type ", (fun id -> TVar.create ~id ())

let get_var (lbl,kind,_) env (x,l) =
  try let i,n = Hashtbl.find lbl#!env x in
      let () = Hashtbl.replace lbl#!env x (i,n+1) in i
  with Not_found -> r_error l "Undefined %svariable <%s>" kind x


let unique kind vars =
  let decls =
    M.empty
    |> List.fold (fun (x,l) m ->
           M.add x (if M.mem x m then l::M.find x m else [l]) m
         ) vars
    |> M.filter_map (fun x -> function
           | a::b::l when x <> "" -> Some (a,b::l)
           | _ -> None)
  in
  if M.is_empty decls |> not then (
    M.iter (fun x (a,l) ->
        p_error a "Multiple declarations of %svariable <%s>" kind x;
        List.iter (fun l -> p_note l "Previously declared here.") l
      ) decls;
    raise Error
  )

let unused (_,_,create) fn env (var,loc) =
  if var = "" then create "_", loc else fn env (var,loc)

(* Environment updator with
   - Name clash detection
   - Unsused variable detection *)
let add_var (lbl,kind,create) env vars fn =
  unique kind vars;

  List.iter (fun (x,_) -> if x <> "" then Hashtbl.add lbl#!env x (create x,0)) vars;
  let r = fn env in
  List.iter (fun (x,l) ->
      if x <> "" then (
        if Hashtbl.find lbl#!env x |> snd = 1 then
          p_warning l "Unused %svariable <%s>" kind x;
        Hashtbl.remove lbl#!env x)) vars;
  r



let opt f d env = function
  | None,_ -> d ()
  | Some i,_ -> f env i


let scope htbl v b f a =
  let () = Hashtbl.add htbl v b in
  let r = f a in
  let () = Hashtbl.remove htbl v in
  r

let rec eta env = opt eta' new_size env and eta' env =
  fst#.(function
        | S_int n -> Size.scal (fst n)
        | S_var x -> Size.unit (get_var siz_k env x)
        | S_add (s1,s2) -> Size.(eta env s1 + eta env s2)
        | S_mul (s1,s2) -> Size.(eta env s1 * eta env s2)
  )

let rec tau env = opt tau' new_type env and tau' env =
  fst#.(function
        | T_base T_int  -> int
        | T_base T_bool -> bool
        | T_var x -> T_var (get_var typ_k env x)
        | T_size s -> size (eta env s)
        | T_index s -> index (eta env s)
        | T_arrow (t,u) -> tau env t => tau env u
  )

let loc f env o = f env o, snd o

let var env x = get_var var_k env x, snd x

let param eta tau env = function
  | Size s -> Size (loc eta env s)
  | Type t -> Type (loc tau env t)

let (let+) f = f

let poly eta tau env = function
  | None  , _ -> P_var (PVar.create ())
  | Some p, l -> P_def (map_fst (List.map (param eta tau env)) p)

let rec exp env (exp,loc) = exp' env exp, loc
and exp' env : _ -> A.exp' = function
  | E_var (x,i)     -> E_var (var env x, ins env i)
  | E_cst c         -> E_cst c
  | E_vaS s         -> E_vaS (loc eta env s)
  | E_coe (e,t)     -> E_coe (exp env e, loc tau env t)
  | E_typ (e,t)     -> E_typ (exp env e, loc tau env t)
  | E_app (f,e)     -> E_app  (exp env f, exp env e)
  | E_abs ((x,t),e) -> let+ env = add_var var_k env [x] in
                       A.E_abs ((unused var_k var env x, loc tau env t), exp env e)

  | E_exS ((i,s),e) -> let s = exp env s in
                       let+ env = add_var siz_k env [i] in
                       A.E_exS ((unused siz_k (loc (get_var siz_k)) env i, s), exp env e)

  | E_cnd (c,b) -> E_cnd (exp env c, Pair.map (exp env) b)
  | E_fix (d,i) -> let+ d,env = decl true  env d in A.E_fix (d, ins env i)
  | E_let (d,e) -> let+ d,env = decl false env d in A.E_let (d, exp env e)
  | E_err -> E_err

and ins env = poly eta tau env
and gen env = poly (get_var siz_k) (get_var typ_k) env

and decl rec_f env (g,x,t,e) fn =
  let v = match fst g with Some v -> fst v | _ -> [] in

  if rec_f then
    let+ env = add_var var_k env [x] in
    let (g,t,e) =
      let+ env = add_var siz_k env (List.filter_map (function Size v -> Some v | _ -> None) v) in
      let+ env = add_var typ_k env (List.filter_map (function Type v -> Some v | _ -> None) v) in
      (gen env g, loc tau env t, exp env e)
    in
    fn ((g,var env x,t,e), env)
  else
    let (g,t,e) =
      let+ env = add_var siz_k env (List.filter_map (function Size v -> Some v | _ -> None) v) in
      let+ env = add_var typ_k env (List.filter_map (function Type v -> Some v | _ -> None) v) in
      (gen env g, loc tau env t, exp env e)
    in
    let+ env = add_var var_k env [x] in
    fn ((g,unused var_k var env x,t,e), env)


exception EndDecl of A.implementation

let rec top_decl l env = function
  | [] -> raise (EndDecl (List.rev l))
  | (d,t)::ds ->
     let (_,(name,_),_,_) = fst d in
     let svars,tvars = AnonymousVars.decl (fst d) (S.empty,S.empty) in
     let+ env = add_var siz_k env (S.fold (fun o -> o,dummy_loc)#.List.cons svars []) in
     let+ env = add_var typ_k env (S.fold (fun o -> o,dummy_loc)#.List.cons tvars []) in
     let+ d,env = decl false env (fst d) in
     let () = if fails t then Hashtbl.remove expr_vars#!env name in
     let svars = S.fold (fun v -> get_var siz_k env (v,dummy_loc))#.SSet.add svars SSet.empty in
     let tvars = S.fold (fun v -> get_var typ_k env (v,dummy_loc))#.TSet.add tvars TSet.empty in
     top_decl (((d,(svars,tvars)),t)::l) env ds

let implementation l =
  let init = Hashtbl.create 127 in
  List.iter (fun ((s,i),_) -> Hashtbl.add init s (i,0)) Builtins.builtins;

  try top_decl [] { expr_vars = init;
                    size_vars = Hashtbl.create 127;
                    type_vars = Hashtbl.create 127 } l |> ignore; assert false
  with EndDecl l -> l
