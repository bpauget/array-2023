open Std
open Fields



module Option = struct
  include Option
(*  include Iterator (Option)*)

  exception Non_none
  exception Non_some

  let fold f = function
    | None -> id
    | Some a -> f a
  let fold2 f a b = fold (fun a -> fold (f a) b) a

  let prop a =
    let add e _ = some e in
    let rem () _ = None in
    F1.( prop a
         |> p_add (a1 add)
         |> p_rem (a1 rem))

  let prop' a =
    let add e = function
      | None -> some e
      | _ -> raise Non_none in
    let rem () = function
      | Some _ -> None
      | _ -> raise Non_some in
    F1.( prop a
         |> p_add (a1 add)
         |> p_rem (a1 rem))


  let pp f p =
    Option.iter (f p)

end

let option  a = Option.prop  a
let option' a = Option.prop' a


