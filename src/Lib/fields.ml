(** Functional Fields

    This module is designed to provide a convenient way to write
    functional code while dealing with records and other data types
    without having to use edition patterns such as
       { record with field = exp }
    They encaplustate both access and edition function.

    Here are the major design traits:
    - Composability: Fields can be chained to access arbitrarily
        deeply nested data (with operator #--)
    - Associativity: This above composition is associative
    - First class: They are just as any Ocaml value. This enables
        to write polymorphic functions that may use any valid field
        on a structured data (given they have the same type)
    - Polymorphism: some traits may be defined on structured data
        (insertion/removal of value, item access, ...) and used with
        polymorphic operators
    - Editable or not: read-write and read-only fields are handled in
        a unified way.

    - Polymorphism limitation: edition cannot change type currently.
        Should be relaxed ?


    To avoid the need of parenthesis, most field operators start with
    a '#', whose priority is higher than application

    These fields are built in two parts:
    1. The accessor functions
    2. The property management

    Error handling: To be fully associative with composition, the
        element accessor (#..) are check at field use. Field f_nop
        garanties structure properties to come from succeding fields.
        This is useful when debugging field errors as it locate the
        error at the place where some properties are missing.

    Extra () argument: To ensure fields to be polymorphic


    ---------------- Ocaml types do not compute. Almost ----------------
    A simple trick to implement AND operation on type flags.
    - flags are represented with a pair of types: 'f * 'g
    - TRUE flags are of the form 'a * a'
    - FALSE flag is defined with type __ * xx (were __ and xx are distinct types)
    - AND operation is defined as : 'f * 'g -> 'i * 'j -> ('f * 'i) * ('f * 'j)

    Notes:
    - flag types may be abstract
    - Type Annotations

    This trick is implemented in the Flag module below.
    The module isn't use in the implementation of fields to provide
    more explicit flags and avoid dummy flag data.
    - o: False flag
    - x: True flag
    - &: And operation
    - !: Flag checker
 **)
module Flag : sig
  type x
  type o
  type 'a t
  val o : (x*o) t
  val x : (x*x) t
  val (&) : ('f*'g) t -> ('i*'j) t -> (('f*'i)*('g*'j)) t
  val (!) : ('a*'a) t -> unit
end =

  struct
    type x
    type o
    type 'a t = F
    let o, x = F,F
    let (&) _ _ = F
    let (!) _ = ()
  end

(*
let test =
  let open Flag in
  (* (* INVALID: ! *)
     ! (x & o) *)
  ! (x & x & x)
*)

(*


      Non-recursive data as a tree, access: path in this tree:
                  ╭────label────╮
                  │ ╭──╴    ╭──╴│
      data |> ╸╸╸╸━━╅──╴┏━━━┿━━━━╺╺╺╺ OP args...
                  │ ┗━━━┹─╴ ╰──╴│
                  ╰─────────────╯

      Properties are added by record fields label while
      accessing elements remove them
      Properties are extracted when label composition stops
      (label stops)
      two cases:
      - Current properties is define by one of steps of label
      - Current properties is the one of ONE type parameter -> cannot be used !

      type ('a,'b) t = 'a * ('a * 'b) list
      let t_prop a b = B2.prop () a b
      let t_lbl = B2.(id_lbl (pair arg_1 (List.assoc arg_1 arg_2)))

      t_lbl #-- p_snd #-- List.(elt i)           || Structure given by t_lbl: pair (arg_1 arg_2)
      t_lbl #-- p_snd #-- List.(elt i) #-- p_fst || Structure givent by argument 1
      t_lbl #-- p_fst #-- p_fst                  || Structure givent by part of argument 1

 *)


open Std

(** Composable field accessors

    For a chain of fields to be writable, each one must be writable.

    r: type of container data (record)
    a: type of contained data (attribute)

    Choice between
    - getter/setter: seems more atomic
    - getter/editor: Well suited for optimized invariant managing:
        no need to apply pre-edition invariant conservation on any
        get. Only before the editor one (and post-edition invariant
        conservation after changing field).

    Associative composition

    Type inference & recursion: Providing fresh type variables with
    abstr_ro & abstr_rw.

    INFO: All the magic lies in [read*only] (and other signatures)
      Indeed, they force type to be not symmetrical thus to be
      unsusable with writting methods (edit 'assert false'). **)
module Field : sig

  (* Read-only type markers *)
  type read
  type only

  (* Abstract type to hide internal type subtleties *)
  type ('r,'a,'f) t

  (* Read-only / read-write versions *)
  type ('r,'a,'f) ro_t = ('r,'a,read*only) t
  type ('r,'a,'f) rw_t = ('r,'a, 'f * 'f ) t

  (* Function signature for field manipulation *)
  type ('r,'a) getter = 'r -> 'a
  type ('r,'a) setter = 'a -> 'r -> 'r
  type ('r,'a) editor = ('a -> 'a) -> 'r -> 'r

  (* Constructors, adding type constraint *)
  val read_only  : ('r,'a) getter                   -> ('r,'a,_) ro_t
  val read_write : ('r,'a) getter -> ('r,'a) editor -> ('r,'a,_) rw_t

  (* Getter setter for fields *)
  val get  : ('r,'a,_)    t -> ('r, 'a) getter
  val set  : ('r,'a,_) rw_t -> ('r, 'a) setter
  val edit : ('r,'a,_) rw_t -> ('r, 'a) editor

  (* Composition operator. To use with any kind of fields *)
  val (#--) : ('r,'s,'a*'b) t -> ('s,'t,'c*'d) t -> ('r,'t,('a*'c)*('b*'d)) t

  (* To avoid <type variable _ occurs inside _> error, apply theses functions to fields *)
  val abstr_ro : ('r,'a,_)    t -> ('r,'a,_) ro_t
  val abstr_rw : ('r,'a,_) rw_t -> ('r,'a,_) rw_t
end =

  struct
    type read
    type only

    type ('r,'t) getter = 'r -> 't
    type ('r,'a) setter = 'a -> 'r -> 'r
    type ('r,'t) editor = ('t -> 't) -> 'r -> 'r

    type ('r,'a,'f) t =
      | F_ro of ('r,'a) getter
      | F_rw of ('r,'a) getter * ('r,'a) editor

    type ('r,'a,'f) ro_t = ('r,'a,read*only) t
    type ('r,'a,'f) rw_t = ('r,'a, 'f * 'f ) t

    let read_only get = F_ro get
    let read_write get edit = F_rw (get,edit)

    let get = function F_ro f | F_rw (f,_) -> f
    let edit = function F_rw (_,f) -> f | _ -> assert false
    let set l v = edit l (fun _ -> v)

    let (#--) n l = match n,l with
      | F_rw (g,e), F_rw (h,f) -> F_rw (g#.h, f#.e)
      | _ -> F_ro ((get n)#.(get l))

    let abstr_ro l = read_only  (get l)
    let abstr_rw l = read_write (get l) (edit l)
  end



(** Static option **)
(** This module provides options with type flag **)
module SOption : sig

  (* Unkown structure type markers *)
  type undefined
  type structure

  (* Type of flagged options *)
  type ('a,'f) t

  (* Type for non-none options *)
  type ('a,'f) s = ('a,'f*'f) t

  (* Constructors *)
  val some  : 'a -> ('a,_) t
  val none  : (_,undefined * structure) t

  (* Mappers. Edit is specialized for the case of non-none options *)
  val map   : ('a -> 'b) -> ('a,'f) t -> ('b,'f) t
  val edit  : ('a -> 'b) -> ('a, _) s -> ('b, _) t

  (* Option's join preserving flags with an extractor (join id) *)
  val join  : ('a -> ('b, 'f*'g) t) -> ('a, 'h*'i) t -> ('b, ('f*'h)*('g*'i)) t

  (* Value extraction, avalaible only for non-none options *)
  val value : ('a,_) s -> 'a
end =

  struct
    type undefined
    type structure

    type ('a,'f) t =
      | None
      | Some of 'a

    type ('a,'f) s = ('a,'f*'f) t

    let none   = None
    let some a = Some a

    let gen = function
      | None   -> None
      | Some a -> Some a

    let join f = function
      | None   -> None
      | Some a -> f a |> gen

    let value = function
      | None   -> assert false
      | Some a -> a

    let map f = function
      | None   -> None
      | Some a -> Some (f a)

    let edit f = map f
  end



(** Field with properties module **)
(** Provides functional fields with generic operations

    Handling parametric types

    Property definitions:
    F.prop arg... builds a empty prop.
    may be extend with p_...
    For convenience, F0 is named F

    Properties add & rem will be pass label's edit function.
    Thus, they must have the following type:
        ((a->a)->r->r) -> t1 -> ... -> tn -> r -> r
    To convert classical edition function with type
        t1 -> ... -> tn -> a -> a
    F<n> modules provide primitives a<n> where n is the number
    of expected arguments (except the modified structure)


    Extra () argument for 0-arguments types

 **)
module PField = struct
  open SOption

  type ('a,'r,'e,'u) prop =
    { p_add : 'a;
      p_rem : 'r;
      p_elt : 'e;
      p_usr : 'u; }

  let empty_prop = { p_add = (); p_rem = (); p_elt = (); p_usr = ()}
  let edit_prop f p () z = p () z |> edit (map_fst f)

  (* Composition *)
  let (#--) f1 f2 () z =
    let f1,z = f1 () z in
    let f2,z = f2 () z in
    Field.(f1 #-- f2), z

  (* Application with properties *)
  let (#-.) f g =
    let f,p = f () none in
    g (value p |> fst) f

  (* Application without properties *)
  let (#-..) f g =
    let f,_ = f () none in
    g f

  (* Element composition *)
  let (#..) f v () z =
    let f1,z = f () z in
    let p,a = value z in
    let f2,z = p.p_elt v () (some ((),a)) in
    Field.(f1 #-- f2), z
end

(** Field definition internals **)
module Fld = struct
  open PField
  open Field
  let ($) f z = f () z
  let _prop a = SOption.some (empty_prop, a)
  let _arg f = SOption.join snd#.f

  let ro get      p z = read_only  get                               , p () z
  let rw get set  p z = read_write get (fun f e -> set (f (get e)) e), p () z
  let re get edit p z = read_write get edit                          , p () z

  (* Helper for modifiers to map functions with 'object' as last argument
     (ie. of the form 'a1 -> ... -> 'ak -> z -> z) to function with 'object'
     as first argument (ie. of the form z -> 'a1 -> ... -> 'ak -> z)) since
     properties should have this form to be used with infix operators.
     *)
  let a0 f e       = f       |> e
  let a1 f e a     = f a     |> e
  let a2 f e a b   = f a b   |> e
  let a3 f e a b c = f a b c |> e
end

(* The none property: to use to left further structure unspecified.
   No properties can be added *)
let nop () _ = SOption.none

(* The non-parametric property: prevents composition with further non 0-arity fields
   Properties may be added *)
let unitp () _ = Fld._prop ()


(* Contructor module for 0-arity properties / fields *)
module F = struct
  include Fld
  let prop () z = _prop ()
  let f_nop () = re id id (prop)
end

(* Contructor module for 1-arity properties / fields *)
module F1 = struct
  include Fld
  let arg () = _arg id
  let prop a () z = _prop (a$z)
  let f_nop () = re id id (prop nop)
end

(* Contructor module for 2-arity properties / fields *)
module F2 = struct
  include Fld
  let arg_1 () = _arg fst
  let arg_2 () = _arg snd
  let prop a b () z = _prop (a$z, b$z)
  let f_nop () = re id id (prop nop nop)
end

(* Contructor module for 3-arity properties / fields *)
module F3 = struct
  include Fld
  let arg_1 () = _arg (fun (a,_,_) -> a)
  let arg_2 () = _arg (fun (_,b,_) -> b)
  let arg_3 () = _arg (fun (_,_,c) -> c)
  let prop a b c () z = _prop (a$z, b$z, c$z)
  let f_nop () = re id id (prop nop nop nop)
end



open Field
open PField

(* Some facilities to add properties *)
let p_add a = edit_prop (fun p -> { p with p_add = a})
let p_rem r = edit_prop (fun p -> { p with p_rem = r})
let p_elt e = edit_prop (fun p -> { p with p_elt = e})
let p_usr u = edit_prop (fun p -> { p with p_usr = u})

(* Syntax for common operations on fields *)

let f_id () z = read_write id id, z
let (#@) l p = l #-- (fun () -> Fld.re id id p)

let ( #-- ) f g = f #-- g
let ( #.. ) f v = f #.. v
let ( #+ ) l = l #-. (fun p f -> p.p_add (edit f))
let ( #- ) l = l #-. (fun p f -> p.p_rem (edit f))

let ( #= ) l = l #-.. set
let ( #> ) l = l #-.. edit

let ( ~: ) l = l #-.. get
let ( #! ) l = ~:l
let ( @. ) e l = ~:l e

let ( @.. ) e l i = l #.. i #! e

(* For incongruous type errors or forcing type of fields *)
let rw_f l () z = l () z |> map_fst abstr_rw
let ro_f l () z = l () z |> map_fst abstr_ro



(* Fields for pairs *)
let f_fst () = F2.(re fst map_fst arg_1)
let f_snd () = F2.(re snd map_snd arg_2)
let pair a b = F2.prop a b |> p_elt (fun b -> if b then f_fst else f_snd)
let u_pair a = pair a a

