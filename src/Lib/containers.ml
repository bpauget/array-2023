(** Interoperable containers for a parametric module of indentifiers

    Note on signature module choices:
    Across this project, parametrized modules with a custom signature
    are defined with a functorial one (see ex bellow). This enables to
    easily share type and module definitions without adding constraints
    to module signatures that declare abstract type or modules.
    When signature is needed, use [module type of] contruct to recover
    module's signature with its embedded constraints.
    This convention forces to build functors with more arguments (see
    DGraph for example).

    ┎─── Example ────────────────────────────────────────────────────
    ┃
    ┃   module Make :
    ┃   functor (I : Sign) ->
    ┃   functor (M : module type of Func (I)) ->
    ┃   sig
    ┃     ...
    ┃   end =
    ┃     functor (I : Sig) ->
    ┃     functor (M : module type of Func (I)) ->
    ┃     struct
    ┃       ...
    ┃     end
    ┃
    ┖────────────────────────────────────────────────────────────────
 **)




(* Container module, to use with [module type of] construct *)

module Make =
  functor (I : Ident.S) ->
  struct
    module Id     = I
    module Set    = XSet.Make (I)
    module Map    = XMap.Make (I) (Set)
(*    module Tree   = Tree.Make (I) (Set) (Map)
    module Assoc  = Assoc.Make (I) (Set) (Map)
    module DGraph = DGraph.Make (I) (Set) (Map)*)
    include I

    type    set  =    Set.t
    type 'a map  = 'a Map.t
    (*    type 'a tree = 'a Tree.t*)

    let set () = Set.prop ()
    let set' () = Set.prop' ()
    let map ?default a () = Map.prop ?default a ()
    let map' ?default a () = Map.prop' ?default a ()
  end

