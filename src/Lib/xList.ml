open Std
open Fields

(** Extended list module

**)
module List = struct
  include List

  exception Not_unique


  (* Functionnal constructors *)
  let empty = []
  let cons x l = x :: l

  let is_empty = function [] -> true | _ -> false
(*
  let cut n l =
    (function l,a::r -> a::l,r
            | _ -> raise (Failure "out of range")) #* n ([],l) |> map_fst List.rev
  let take n l = fst (cut n l)
  let drop n l = snd (cut n l)
 *)


  (* Helper for pretty printing *)
  let pp s f =
    Format.(pp_print_list ~pp_sep:(fun p () -> fprintf p s)) f

  let dp s f l p =
    Format.(pp_print_list ~pp_sep:(fun p () -> fprintf p s) (fun p e -> f e p)) p l

  (* Convenient fold_left to use as other folds *)
  let fold reduce list init =
    fold_left (fun acc elt -> reduce elt acc) init list

  (* Convenient fold_left2 to use as other folds *)
  let fold2 reduce list1 list2 init =
    fold_left2 (fun acc elt1 elt2 -> reduce elt1 elt2 acc)
      init list1 list2

  (* Mapping list with an accumulator *)
  let map_fold fn = fold_left_map fn


  (* Remove the common start of a list pair *)
  let rec differ ?(cmp=(=)) = function
    | a::l, b::m when cmp a b -> differ ~cmp (l,m)
    | p -> p

  (* Remove the common start of a list pair *)
  let rec common ?(cmp=(=)) = function
    | a::l, b::m when cmp a b -> a :: (common ~cmp (l,m))
    | p -> []


  (* Belonging checker *)
  let check ?(cmp=(=)) expct elt list =
    match List.exists (cmp elt) list, expct with
    | true, false -> raise Not_unique
    | false, true -> raise Not_found
    | _ -> ()

  (* Editor functions *)
  let insert ?cmp elt list = elt :: list
  let rec remove ?(cmp=(=)) elt = function
    |   []   -> []
    | hd::tl -> if cmp elt hd then tl else hd :: remove ~cmp elt tl

  (* Strict version of editor functions *)
  let insert' ?cmp elt list = check ?cmp false elt list; insert ?cmp elt list
  let remove' ?cmp elt list = check ?cmp true  elt list; remove ?cmp elt list

  (* Element accessors *)
  let get_nth n l = nth l n
  let rec edit n f l = match n with
    | 0 -> f (hd l) :: tl l
    | n -> hd l :: edit (n-1) f (tl l)

  (* Field for nth element *)
  let f_nth n () = F1.(re (get_nth n) (edit n) arg)

  (* Field properties for list *)
  let prop  ?cmp a = F1.(prop a |> p_add (a1 (insert  ?cmp)) |> p_rem (a1 (remove  ?cmp)) |> p_elt f_nth)
  let prop' ?cmp a = F1.(prop a |> p_add (a1 (insert' ?cmp)) |> p_rem (a1 (remove' ?cmp)) |> p_elt f_nth)
end


let list  a = List.prop  a
let list' a = List.prop' a

