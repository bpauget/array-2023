


type ___
type __ = F___ of ___ (* Flag undef *)
type xx = F_xx of ___ (* Flag true  *)


type ('a,'b,'f) alternative =
  | A_fst : 'a -> ('a,_,__) alternative
  | A_snd : 'b -> (_,'b,xx) alternative

let (~*.) (A_fst a) = a
let (~.*) (A_snd a) = a

type ('a,'f) f_option = (unit,'a,'f) alternative
let no_opt = A_fst ()
let (?*) o = ~.*o
let opt o = ?*o
let (~?) o = A_snd o

let (~%) f = Format.asprintf f
let (~!) f = f ()


let id x = x
let cst c _ = c
let ign _ = id
let (#.) f g a = g (f a)
let (<|>) f g a = g (f a)


let   curry f a b = f (a,b)
let uncurry f (a,b) = f a b

type 'a pair = 'a * 'a

let map_fst f (a,b) = (f a, b)
let map_snd f (a,b) = (a, f b)


(* Iterated application *)
let rec ( #* ) f n a = if n = 0 then a else f#*(n-1) (f a)



let print s n = Format.printf "%s" s; n
let (~%) f = Format.asprintf f





module type Cont = sig
  type 'a t
  val split : ('a * 'b) t -> 'a t * 'b t
  val map_fold : ('a -> 'b -> 'a * 'c) -> 'a -> 'b t -> 'a * 'c t
  val map_fold2 : ('a -> 'b -> 'c -> 'a * 'd) -> 'a -> 'b t -> 'c t -> 'a * 'd t
end


module Iterator (C: Cont) = struct
  include C
  let map f c = map_fold (fun () v -> (), f v) () c |> snd
  let fold f c a = map_fold (fun a v -> f v a, ()) a c |> fst
  let iter f c = fold (fun v () -> f v) c ()

  let map2 f c d = map_fold2 (fun () v w -> (), f v w) () c d |> snd
  let fold2 f c d a = map_fold2 (fun a v w -> f v w a, ()) a c d |> fst
  let iter2 f c d = fold2 (fun v w () -> f v w) c d ()
end

module Pair =
  Iterator (struct
      type 'a t = 'a * 'a

      let split ((a,b),(c,d)) = (a,c),(b,d)
      let map_fold f a (b,c) =
        let a,b = f a b in
        let a,c = f a c in
        a,(b,c)
      let map_fold2 f a (b1,c1) (b2,c2) =
        let a,b = f a b1 b2 in
        let a,c = f a c1 c2 in
        a,(b,c)
    end)


(* Container Composition *)
module CC (A:Cont) (B:Cont) =
  Iterator (struct
      module B = Iterator (B)
      type 'a t = 'a A.t B.t
      let split c = B.map A.split c |> B.split
      let map_fold f = B.map_fold (A.map_fold f)
      let map_fold2 f = B.map_fold2 (A.map_fold2 f)
    end)


let debug i = if false then Format.printf i else ()



(** User define comparison: How to compare data with partially irrelevant
    content (e.g. location) **)
module Order : sig
  type 'a rel  = 'a -> 'a -> bool
  type 'a comp = 'a -> 'a -> int
  type 'a part

  (** Lexicographic ordering **)
  val (=<>) : int -> int -> int
  val lex : 'a comp -> 'a -> 'a -> int -> int
  val (~@) : 'a comp -> 'a -> 'a -> int -> int

  (** Conversion to boolean operators **)
  val ( ~=  ) : 'a comp -> 'a rel
  val ( ~<  ) : 'a comp -> 'a rel
  val ( ~>  ) : 'a comp -> 'a rel
  val ( ~<> ) : 'a comp -> 'a rel

  (** Comparison with a specified [compare] function:
      usage: exp1 OP@<comp>@ exp2
      where OP is one of: = < > <>
            comp has type 'a -> 'a -> int
            exp1 & exp2 have type 'a
      The compound operator [OP@<comp>@] has the same precedence
      as Ocaml [OP] operator.
   **)
  val ( =@<) : 'a -> 'a comp -> 'a part
  val ( <@<) : 'a -> 'a comp -> 'a part
  val ( >@<) : 'a -> 'a comp -> 'a part
  val (<>@<) : 'a -> 'a comp -> 'a part
  val (  >@) : 'a part -> 'a -> bool
end =
  struct
    type 'a rel  = 'a -> 'a -> bool
    type 'a comp = 'a -> 'a -> int
    type 'a part = 'a -> bool

    let (=<>) a b = if a = 0 then b else a
    let lex f a b c = c =<> f a b

    let apply o a c b = o (c a b) 0
    let ( =@<) a = apply ( =) a
    let ( <@<) a = apply ( <) a
    let ( >@<) a = apply ( >) a
    let (<>@<) a = apply (<>) a
    let (  >@) f b = f b
    let ( ~@ ) f = lex f

    let convert o c a = apply o a c
    let ( ~=  ) c = convert ( =) c
    let ( ~<  ) c = convert ( <) c
    let ( ~>  ) c = convert ( >) c
    let ( ~<> ) c = convert (<>) c
  end

include Order


module Print = struct
  include Format
  let opt f p = function
    | Some e -> fprintf p "Some (%a)" f e
    | None   -> fprintf p "None"
  let pair f g p (a,b) = fprintf p "%a, %a" f a g b
  let unit p () = fprintf p "()"
  let any p _ = fprintf p "?"
end



