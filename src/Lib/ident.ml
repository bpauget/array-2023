(** Global identifier abstraction

    Used across containers
    Conventions:
    Module.prop: label
    Module.prop': strict label

 **)


module type S = sig
  include Map.OrderedType
  exception Not_found of t
  exception Not_unique of t
end



(** Most simple implementation with strings **)
module SIdent = struct
  type t = String.t
  let compare = String.compare
  exception Not_found of t
  exception Not_unique of t
end
