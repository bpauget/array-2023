
open Std


(** Monad signature and related operators **)
module Monad = struct

  (** Monad signature **)
  module type S = sig
    type 'a t
    val return : 'a -> 'a t
    val bind : 'a t -> ('a -> 'b t) -> 'b t
  end

  (** Operators (let*, >>=, ...) introduction **)
  module Intro (M: S) = struct
    include M
    let (>>=) e f = bind e f
    let (>>-) e f = bind e f#.return
    let (let*) e f = bind e f
    let (let+) e f = bind e f#.return
    let (and*) a b = let* a = a in
                     let+ b = b in a,b
    let (and+) a b = (and*) a b

    let rec mapM f = function
      | [] -> return []
      | a::l -> let+ a = f a
                and+ l = mapM f l in a::l

    let rec foldM f = function
      | [] -> return
      | a::l -> fun e -> f a e >>= foldM f l

    let rec iterM f = function
      | [] -> return ()
      | a::l -> let+ () = f a
                and+ () = iterM f l in ()
  end
end



(** State monad and utilities **)
module StateMonad = struct

  (** State monad signature **)
  module type S = sig
    include Monad.S
    type s
    type effect = unit t
    val effect : (s -> s) -> effect
    val read : (s -> 'a) -> 'a t
    val init : s -> 'a t -> 'a * s
    val process : s -> unit t -> s
  end

  (** Canonical state monad implementation **)
  module Make (S: sig type t end) = struct
    type s = S.t
    type 'a t = s -> 'a * s
    type effect = unit t
    let effect f res = (), f res
    let read f s = f s, s
    let init s c = c s
    let process s c = init s c |> snd
    let return a res = a,res
    let bind a f res =
      let a,res = a res in
      f a res
  end

  (** Operators (let*, >>=, ...) introduction **)
  module Intro (M: S) = struct
    include M
    include Monad.Intro (M)

    let (let<>) (b,e) f =
      let+ () = b
      and+ r = f ()
      and+ () = e in r

    let (let<->) pe = pe

  end



  module Pair (Fst: S) (Snd: S) = struct
    module M = Make (struct type t = Fst.s * Snd.s end)
      include Intro (M)

      let e_fst eff = effect (map_fst (fun s -> let (),s = Fst.init s eff in s))
      let e_snd eff = effect (map_snd (fun s -> let (),s = Snd.init s eff in s))

      let b_fst blk = Pair.map e_fst blk
      let b_snd blk = Pair.map e_snd blk
  end


  module Empty  = struct
    type s = unit
    type 'a t = 'a
    type effect = unit t
    let effect _ = ()
    let read f = f ()
    let init _ a = a, ()
    let process _ = id
    let return a = a
    let bind a f = f a
  end

end


