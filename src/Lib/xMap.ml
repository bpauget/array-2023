open Fields

(** Extended Set Module

    Provide inter-operability with labels
 **)
module Make =
  functor (Key : Ident.S) ->
  functor (KSet : module type of XSet.Make (Key)) ->
  struct

    include Map.Make (Key)


    (* Belonging checker *)
    let check expct key map =
      match mem key map, expct with
      | true, false -> raise (Key.Not_unique key)
      | false, true -> raise (Key.Not_found key)
      | _ -> ()

    let find ?default ?fallback key map =
      match find_opt key map with
      | Some v -> v
      | None -> match default, fallback with
                | None, Some f -> f key
                | Some v, None -> v
                | _ -> raise (Key.Not_found key)

    (* Strict version of editor functions *)
    let add' key value map = check false key map; add key value map
    let remove' key map = check true key map; remove key map
    let replace key value map = check true key map; add key value map

    let get_opt map key = find_opt key map
    let get ?default ?fallback map key = find ?default ?fallback key map
    let edit f key map = check true key map;  add key (f (find key map)) map

    let assoc dflt key map = match find_opt key map with
      | None -> dflt key
      | Some v -> v

    let pp s f p m = XList.List.pp s f p (fold (fun i t -> List.cons (i,t)) m [])
    let dp s f m = XList.List.dp s (Std.uncurry f) (fold (fun i t -> List.cons (i,t)) m [] |> List.rev)

    let union_merger f a b = match a,b with
      | o,None | None,o -> o
      | Some a , Some b -> Some (f a b)

    let union f = merge (fun _ -> union_merger f)
    let union' a = merge (fun i -> union_merger (fun _ _ -> raise (Key.Not_unique i))) a

    let build f s = KSet.fold (fun k -> add k (f k)) s empty

    let keys m = fold (fun k _ -> KSet.add k) m KSet.empty



    let elt ?default i ()  = F1.(rw (find ?default i) (add i) arg)

    let prop  ?default a = F1.(prop a |> p_add (a2 add ) |> p_rem (a1 remove ) |> p_elt (elt ?default))
    let prop' ?default a = F1.(prop a |> p_add (a2 add') |> p_rem (a1 remove') |> p_elt (elt ?default))
  end
