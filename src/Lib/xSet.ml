open Fields

(** Extended Set Module

    Provide inter-operability with labels
 **)
module Make =
  functor (Key : Ident.S) ->
  struct

    include Set.Make (Key)

    (* Belonging checker *)
    let check expct key set =
      match mem key set, expct with
      | true, false -> raise (Key.Not_unique key)
      | false, true -> raise (Key.Not_found key)
      | _ -> ()

    (* Strict version of editor functions *)
    let add'    key set = check false key set; add key set
    let remove' key set = check true key set; remove key set

    (* Labels *)
    let prop ()  = F.(prop |> p_add (a1 add ) |> p_rem (a1 remove )) ()
    let prop' () = F.(prop |> p_add (a1 add') |> p_rem (a1 remove')) ()

  end
