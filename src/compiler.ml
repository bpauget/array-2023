open Std

type lang =
  | L_core
  | L_surf

let lang = ref None
let lang_list = [ L_core, "Core"
                ; L_surf, "Surf"
                ]

let get_lang arg =
  try lang := Some (fst (List.find snd#.(String.compare arg)#.((=) 0) lang_list))
  with Not_found -> raise (Arg.Bad "Please specify input language")


let infer = ref 0
let infer_types = "types" , 2
let infer_refin = "refine", 3
let infer_sizes = "sizes" , 4
let infer_none  = "none"  , 5

let error i p e l =
  p_error l "%a" p e;
  close_in i;
  exit (1)

let syntax_error p () = Format.fprintf p "Syntax: unexpected token"
let lexing_error p s = Format.fprintf p "Syntax: %s" s



let no_dump = ref false
let dump_sign = ref true
let dump_elab = ref false
let dump_subs = ref false

let (#<<<) cnt =
  (if !cnt && not !no_dump then Format.fprintf else Format.ifprintf)
    (Format.formatter_of_out_channel stdout)



(* Effect reversed application & forwarding *)
let (|-) a f = f a; a

(* Declaration printer *)
let p_decl vars (d,_) = Format.printf "%a@." (Core.Printer.decl "let" vars) d



(* Test status handler *)
let process_decl (type a b) pr env (decl,test) =
  let exception Return of (a*b) in
  let n_fail,loc = Option.value ~default:(true,dummy_loc) test in
  if not n_fail then Format.printf "@{<C 0;92>⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅ Failure expected ⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅@}@.";
  try raise (Return (pr env decl)) with

  | Return (env,d) ->
     if n_fail then env,(d,test)
     else (Format.printf "@{<C 0;92>⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅@}@.";
           Std.r_error loc "This declaration should not pass static checks")

  | Std.Error ->
     if n_fail then raise Std.Error;
     Format.printf "@{<C 0;92>⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅⋅@}@.";
     dump_elab #<<< "@.";
     env,(decl,test)




(* Single core declaration analysis *)
let core_decl env decl =
  let vars = (snd ~!SVar.ids, snd ~!TVar.ids) in

  let env,(g,i,s,t) = Core.Inference.top_decl env decl in

  SMap.iter (fun v -> dump_subs #<<< "[Subs size] %s => %a@." (fst vars v) (Core.Printer.eta vars)) s;
  TMap.iter (fun v -> dump_subs #<<< "[Subs type] %s => %a@." (snd vars v) (Core.Printer.tau vars)) t;

  let decl =
    map_fst (id
             <|> Core.Utils.MapG.decl g
             <|> Core.Utils.MapI.decl i
             <|> Core.Utils.MapS.decl s
             <|> Core.Utils.MapT.decl t) decl
  in

  let (g,x,t,_) = fst decl in

  dump_sign #<<< "val %a: %a@." p_ident (fst x) (Core.Printer.pat vars)
                   (match g with P_def l -> (fst l,t) | _ -> assert false);

  env, decl




(* Core file typing *)
let process_core file =
  let open Core in

  let ic = open_in file in
  let lexbuf = Lexer.init file ic in
  try
    lexbuf
    |> Parser.program Lexer.token
    |> NameResolution.implementation
    |> List.map_fold (process_decl core_decl) Inference.init_env |> snd
    |> ignore
  with
  | Lexer_error s -> error ic lexing_error s (Lexer.position lexbuf)
  | Parser.Error          -> error ic syntax_error () (Lexer.position lexbuf)
  | Std.Error             -> (close_in ic; exit 1)
  | x -> (close_in ic; raise x)



(* Single surface declaration analysis *)
let surf_decl env decl =
  let vars = (snd ~!SVar.ids, snd ~!TVar.ids) in

  dump_elab #<<< "%a@." (Surface.Printer.decl vars) (fst decl);
  let _, decl = Surface.Inference.top_decl (env, IMap.empty) decl in
  dump_elab #<<< "%a@." (Surface.Printer.decl vars) (fst decl);


  let c_decl = Surface.Elaboration.top_decl decl in
  dump_elab #<<< "%a@." Core.Printer.top_decl (c_decl,());

  let env,(g,i,s,t) = Core.Inference.top_decl env c_decl in


  SMap.iter (fun v -> dump_subs #<<< "[Subs size] %s => %a@." (fst vars v) (Core.Printer.eta vars)) s;
  TMap.iter (fun v -> dump_subs #<<< "[Subs type] %s => %a@." (snd vars v) (Core.Printer.tau vars)) t;


  let decl =
    map_fst (id
             <|> Surface.Utils.MapG.decl g
             <|> Surface.Utils.MapI.decl i
             <|> Surface.Utils.MapS.decl s
             <|> Surface.Utils.MapT.decl t) decl
  in

  let (_,g,x,(_,t),_) = fst decl in

  dump_sign #<<< "val %a: %a@." p_ident (fst x) (Surface.Printer.pat vars)
                   (match g with P_def l -> (fst l,t) | _ -> assert false);

  dump_elab #<<< "-----------------------------@.";

  env, decl

(* Core file typing *)
let process_surf file =
  let open Surface in
  let ic = open_in file in
  let lexbuf = Lexer.init file ic in
  try
    lexbuf
    |> Parser.program Lexer.token
    |> NameResolution.implementation
    |> List.map_fold (process_decl surf_decl) Inference.init_env |> snd
    |> ignore
  with
  | Lexer_error s -> error ic lexing_error s (Lexer.position lexbuf)
  | Parser.Error          -> error ic syntax_error () (Lexer.position lexbuf)
  | Std.Error             -> (close_in ic; exit 1)
  | x -> (close_in ic; raise x)


let process arg =
  match !lang with
  | None -> get_lang arg
  | Some a -> match a with
              | L_core -> process_core arg
              | L_surf -> process_surf arg


let usage_msg =
  "Usage:\n"
  ^(List.fold_left (fun s (_,n) -> s^"  prgm [options] "^n^" <sources>\n") "" lang_list)
  ^"Options:\n"

let speclist =

  [(* "--infer-types",
    Arg.Unit (fun () ->
        if !lang <> Some L_core then raise (Arg.Bad "This option is available for Core mode only");
        if !infer <> 0 then raise (Arg.Bad "Multiple inference setting specified");
        infer := snd infer_types),
    "    Start inference at type inference (Core language only)"

  ; "--infer-refine",
    Arg.Unit (fun () ->
        if !lang <> Some L_core then raise (Arg.Bad "This option is available for Core mode only");
        if !infer <> 0 then raise (Arg.Bad "Multiple inference setting specified");
        infer := snd infer_refin),
    "    Start inference at refinement inference (Core language only)"

  ; "--infer-sizes",
    Arg.Unit (fun () ->
        if !lang <> Some L_core then raise (Arg.Bad "This option is available for Core mode only");
        if !infer <> 0 then raise (Arg.Bad "Multiple inference setting specified");
        infer := snd infer_sizes),
    "    Start inference at size inference (Core language only)"

  ; "--infer-none",
    Arg.Unit (fun () ->
        if !lang <> Some L_core then raise (Arg.Bad "This option is available for Core mode only");
        if !infer <> 0 then raise (Arg.Bad "Multiple inference setting specified");
        infer := snd infer_none),
    "    Only type check sources (Core language only)"

  ;*) "--no-dump",
    Arg.Set no_dump,
    "This parameter overrides other dumping settings"

  ; "--dump-elab",
    Arg.Set dump_elab,
    "Print result of size parameter insersion and elaboration"

  ; "--dump-subs",
    Arg.Set dump_subs,
    "Print result of substitution"
]


let _ = Arg.parse (Arg.align speclist) process usage_msg
