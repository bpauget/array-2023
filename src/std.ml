(* The module Std contains shared definitions used across multiple
   parts of the compiler *)



open Lib
include Std
include Fields
include Monads
include XOption
include XList



exception Lexer_error of string


let cpt () =
  let n = ref 0 in
  let get () =
    n := !n + 1; !n
  in get





module I = Containers.Make (Ident.SIdent)


module Int = struct
  include Int
  exception Not_found of t
  exception Not_unique of t
end

module IC = Containers.Make (Int)
module ISet = IC.Set
module IMap = IC.Map
type iset = ISet.t
type 'a imap = 'a IMap.t
let iset  () = ISet.prop  ()
let iset' () = ISet.prop' ()
let imap  a = IMap.prop  a
let imap' a = IMap.prop' a


(**---- Source locations ----------------**)

module Color = struct
  let red = "1;91"
  let cyan = "1;96"
  let white = "1;97"
  let yellow = "1;33"
end

let str_to_esc_seq = function
  | Format.String_tag tag ->
     if tag.[0] = 'C' then String.split_on_char ' ' tag |> List.tl |> List.hd |> Option.some else None
  | _ -> None

let color_stag_funs =
  let open Format in
  { mark_open_stag = str_to_esc_seq#.Option.(map (sprintf "\027[%sm"))#.Option.(value ~default:"");
    mark_close_stag = (fun _ -> "\027[0m");
    print_open_stag = (fun _ -> ());
    print_close_stag = (fun _ -> ()); }

let () = Format.set_formatter_stag_functions color_stag_funs
let () = Format.set_mark_tags true




(** To convert __POS__ to it's start location **)
let pos_loc (f,l,c,_) = f,l,c

(* Source location *)
type location =
  { l_file : string;
    l_lbeg : int;
    l_lend : int;
    l_cbeg : int;
    l_cend : int; }

(* Invalid location *)
let dummy_loc =
  { l_file = "";
    l_lbeg = -1;
    l_lend = -1;
    l_cbeg = -1;
    l_cend = -1; }

(* Location merger: locations must target the same file *)
let extend li lf =
  if      li = dummy_loc then lf
  else if lf = dummy_loc then li
  else begin
      assert (li.l_file = lf.l_file);
      if li.l_lend = lf.l_lend
      then { li with l_cend = lf.l_cend }
      else { li with l_cend = lf.l_cend;
                     l_lend = lf.l_lend(*;
                     l_ebol = lf.l_ebol*) }
    end


(* Location merger operator *)
let (<->) li lf = extend li lf

(* Location OCaml's style pretty printer *)
let output_location p l =
  let open Format in
  if l.l_lbeg = l.l_lend
  then fprintf p "@{<C %s>File \"%s\", line %d, characters %d-%d@}" Color.white
         l.l_file l.l_lbeg l.l_cbeg l.l_cend
  else fprintf p "@{<C %s>File \"%s\", lines %d-%d, characters %d-%d@}" Color.white
         l.l_file l.l_lbeg l.l_lend l.l_cbeg l.l_cend

exception Error

let _SEP_IN = not true
let _SEP_OUT = true

let pp_loc cnt prefix color title fmter loc format =
  let open Format in
  kfprintf (fun fmter ->
      kfprintf (fun fmter ->
          kfprintf cnt fmter (if _SEP_OUT then "@]@." else "@]")
        ) fmter format)
    fmter
    (if _SEP_IN then "@.%s%a:@.%s@{<C %s>%s@}: @[<v>" else "%s%a:@.%s@{<C %s>%s@}: @[<v>")
    prefix output_location loc prefix color title
  
(* Print error message and raise exception *)
let p_warning loc = pp_loc ignore "" Color.yellow "Warning" Format.std_formatter loc
let p_error   loc = pp_loc ignore "" Color.red    "Error"   Format.std_formatter loc
let p_note    loc = pp_loc ignore "  " Color.cyan "Note"  Format.std_formatter loc
let r_error   loc = pp_loc (fun _ -> raise Error) "" Color.red "Error"   Format.std_formatter loc


(**---- Core definitions ----------------**)

type ident = int

(* Name handling *)
module Names : sig
  val to_string : int -> string
  val create : string -> int
  val rename : int -> string -> unit
end =
  struct
    let names = Hashtbl.create 997
    let () = Hashtbl.add names 0 ""
    let id_nb = ref 0

    let create name =
      id_nb := !id_nb + 1;
      Hashtbl.add names !id_nb name;
      !id_nb

    let rename = Hashtbl.replace names

    let to_string i = match Hashtbl.find_opt names i with
      | Some s -> ~%"%s" (if s = "" then "_" else s)
      | None   -> ~%"_%d" i
  end

let p_ident p i = Format.fprintf p "%s" (Names.to_string i)





(* Convenient aliases *)
type 'a loc = 'a * location
type 'a opt = 'a option


module TOrder (C : sig type t
                       val cmp : t -> t -> int
                   end) =
  struct
    include C
    let eq a b = cmp a b = 0
    let lt a b = cmp a b < 0
    let gt a b = cmp a b > 0
    let le a b = cmp a b <= 0
    let ge a b = cmp a b >= 0
    let ne a b = cmp a b <> 0
    let min a b = if a < b then a else b
    let max a b = if a > b then a else b
  end



module type Vars_S = sig
  open Lib
  module I : Ident.S
  include module type of Containers.Make (I)

  val ids : unit -> (t -> string -> unit) * (t -> string)
  val undef : t
  val create : ?id:string -> unit -> t
end

(* Typed integer variables *)
module Vars (R:sig
                 val shft : int
                 val pref : string
               end) : Vars_S =

  struct
    let names = ref IMap.empty

    (* Identifier mapping *)
    let n_names ?(shft=0) pref =
      let a_names = ref IMap.empty in

      let rec new_id ?(nb=1) id =
        let idn = if nb < 2 then id else id^(string_of_int nb) in
        if IMap.exists (fun _ n -> idn = n) !a_names || IMap.exists (fun _ n -> idn = n) !names
        then new_id ~nb:(nb+1) id
        else idn
      in

      let insert i n = a_names := IMap.add i (new_id n) !a_names in
      let get i =
        if i = 0 then "?" else
          match IMap.find_opt i !names with
          | Some n -> n
          | _ ->  match IMap.find_opt i !a_names with
                  | Some n -> n
                  | _ -> let n = new_id (~%"%s%c" pref "abcdefghijklmnopqrstuvwxyz".[(IMap.cardinal !a_names+shft) mod 26]) in
                    insert i n; n
      in
      insert, get




    module I = Int
    module C = IC
    include I
    include C

    open R
    let ids () = n_names ~shft pref
    let create =
      let c = cpt () in
      fun ?id () ->
      let n = c () in
      names := Option.fold (IMap.add n) id !names;
      n
    let undef = 0
  end



(** Base types **)
type typ =
  | T_int
  | T_bool

let typ_s = function
  | T_int  -> "int"
  | T_bool -> "bool"

(** Literals **)
type cst =
  | C_int  of int
  | C_bool of bool

let cst_s = function
  | C_int  i -> string_of_int i
  | C_bool b -> if b then "true" else "false"

(** Literal typing **)
let cst_typ = function
  | C_int _  -> T_int
  | C_bool _ -> T_bool


(** Size / Type choice **)
type ('s,'t) param =
  | Size of 's
  | Type of 't

let param_map eta tau = function
  | Size s -> Size (eta s)
  | Type t -> Type (tau t)

let param_fold eta tau = function
  | Size s -> eta s
  | Type t -> tau t






(* Type variables *)
module TVar = Vars (struct let shft = 0 let pref = "'" end)
module TSet = TVar.Set
module TMap = TVar.Map

(* Size variables *)
module SVar = Vars (struct let shft = 8 let pref = "'" end)
module SSet = SVar.Set
module SMap = SVar.Map


(* Multivariate polynomials over size variables *)
module MPol = Lib.Polynom.Make (Int) (SVar.I) (SVar)


(* Poly variables *)
module PVar = Vars (struct let shft = 0 let pref = "~" end)
module PSet = PVar.Set
module PMap = PVar.Map


type 'a pvar =
  | P_var of PVar.t
  | P_def of 'a


(* Sizes *)
module Size = struct
  module Var = SVar
  module Set = SSet
  module Map = SMap

  include MPol
  type v = Var.t

  type set = Var.set
  type 'a map = 'a Var.map
  type sub = t map

  let set () = Var.set ()
  let map a = Var.map a

  let create ?id () = unit ~!Var.(create ?id)
  let undef = unit Var.undef

  let equal s1 s2 = s1 =@<compare>@ s2
end
type eta = Size.t


module Prop = struct

  (* Type equality tester *)
  type ('a,'b) eq =
    | Eq: ('a,'a) eq
    | Ne:    _    eq

  (* TODO: move to lib *)
  type    o = private O
  type 'n s = private S

  type 'n v =
    | O:            o v
    | S: 'n v -> 'n s v

  type any = A: _ v -> any

  let rec eq_v: type n p. n v -> p v -> (n,p) eq = fun v1 v2 ->
    match v1, v2 with
    | O, O -> Eq
    | S _, O | O, S _ -> Ne
    | S n, S p -> match eq_v n p with Eq -> Eq | _ -> Ne

  let rec val_v: type n. n v -> int = function
    | O -> 0
    | S n -> 1 + val_v n

end

module NSeq = struct
  open Prop

  type 'a t =
    |  [] : ('a * o) t
    | (::): 'a * ('a*'n) t -> ('a*'n s) t

  type 'a any = A : ('a*'n) t -> 'a any

  let empty = []
  let cons a l = a :: l

  let hd = function a::l -> a
  let tl = function a::l -> l

  let rec repeat: type n. _ -> n v -> (_*n) t = fun f ->
    function
    | O -> []
    | S n -> f () :: repeat f n

  let gen f =
    let rec gen: type n. _ -> n v -> (_*n) t = fun k ->
      function
      | O -> []
      | S n -> f k :: gen (k+1) n
    in gen 0

  let rec len: type n. (_*n) t -> n v = function
    |   []   -> O
    | _ :: s -> S (len s)

  let rec map: type n. _ -> (_*n) t -> (_*n) t = fun f ->
    function
    |   []   -> []
    | e :: s -> f e :: map f s

  let rec fold: type n. _ -> (_*n) t -> _ = fun f ->
    function
    |   []   -> id
    | e :: s -> f e <|> fold f s

  let rec list: type n. (_*n) t -> _ list = function
    |   []   ->   []
    | e :: s -> e :: list s

  let rec combine: type n. (_*n) t -> (_*n) t -> (_*n) t = fun s t ->
    match s,t with
    |  [] ,  [] -> []
    | a::s, b::t -> (a,b) :: combine s t
(*
  let rec map_opt: type n. _ -> (_*n) t -> (_*n) t option = fun f ->
    function
    |   []   -> Some []
    | e :: s -> Option.map2 cons (f e) (map_opt f s)
*)
  let rec map_fold: type n. _ -> _ -> (_*n) t -> _ * (_*n) t = fun f a ->
    function
    |   []   -> a, []
    | e :: s -> let a, e = f a e in
                let a, s = map_fold f a s in
                a, e :: s

  let mapi fn seq =
    map_fold (fun i n -> i+1, fn i n) 0 seq |> snd

  let map2 fn s1 s2 = map (uncurry fn) (combine s1 s2)

  let rec equal: type n p. _ -> (_*n) t -> (_*p) t -> (n,p) eq =
    fun f s1 s2 ->
    match s1,s2 with
    | [], [] -> Eq
    | a1::s1, a2::s2 ->
       (match equal f s1 s2 with Eq when f a1 a2 -> Eq | _ -> Ne)
    | _ -> Ne

  let rec from_list: _ list -> _ = function
    | [] -> A []
    | a::l -> let A l = from_list l in A (a::l)


end

module Types = struct
  open Prop

  (* Type component variance *)
  type variance = Con | Equ | Cov

  (* Integer refinements *)
  type refine =
    | R_top
    | R_siz of Size.t
    | R_idx of Size.t


  (* Type constructors *)
  type 'n t_ctor =
    | T_int: refine                         ->     o t_ctor
    | T_fun:                                   o s s t_ctor
    | T_arr:                                   o s s t_ctor
    | T_tpl: 'n v                           ->    'n t_ctor
    | T_def: ident * (variance * 'n) NSeq.t ->    'n t_ctor

  (* Types *)
  type tau =
    | T_var: TVar.t        -> tau
    | T_val: _ built       -> tau
  and 'n built = 'n t_ctor * (tau * 'n) NSeq.t

  (* Wrapper to unify sizes and types *)
  module Type = struct
    module Var = TVar
    module Set = TSet
    module Map = TMap

    type t = tau
    type v = Var.t

    type set = Var.set
    type 'a map = 'a Var.map
    type sub = t map

    let set () = Var.set ()
    let map a = Var.map a

    let unit i = T_var i
    let to_unit = function
      | T_var i -> Some i
      | _ -> None

    let create ?id () = unit ~!Var.(create ?id)
    let undef = unit Var.undef

    let equal t1 t2 = t1 =@<compare>@ t2
  end


  type 'a typed = 'a * Type.t


  let rec repeat: type n. n v -> _ -> (_*n) NSeq.t = function
    | O   -> fun _ -> []
    | S n -> fun v -> v :: repeat n v

  let variance: type n. n t_ctor -> (_ * n) NSeq.t = function
    | T_int _ -> []
    | T_fun -> [Con;Cov]
    | T_arr   -> [Equ;Cov]
    | T_tpl n -> repeat n Cov
    | T_def (i,v) -> v

  let map_built fn = map_snd (NSeq.map fn)

end

include Types


let i_bool = Names.create "bool"
let int       = T_val (T_int  R_top   , [])
let size s    = T_val (T_int (R_siz s), [])
let index s   = T_val (T_int (R_idx s), [])
let bool      = T_val (T_def (i_bool,[]), [])
let (=>) d c  = T_val (T_fun, [d;c])
let array s t = index s => t (*T_val (T_arr, [index s; t])*)
let t_unit    = T_val (T_tpl O, [])

(*
(* Types *)
type tau =
  | T_var of TVar.t
  | T_base of typ
  | T_size of eta
  | T_index of eta
  | T_arrow of tau pair

(* Wrapper to unify sizes and types *)
module Type = struct
  type t = tau
  type v = TVar.t
  module Map = TMap
  module Set = TSet
  let unit i = T_var i
  let to_unit = function
    | T_var i -> Some i
    | _ -> None
end
*)

(*
let f:type a. a -> a = fun (x:'a) -> x
*)


type 'a tested = 'a * bool loc opt

let fails = function
  | Some (b,_) -> not b
  | _ -> false


let (-|-) f g (a,b) = f a, g b



module Gen = struct
  type t = Size.set * Type.set
  type 'a m = 'a -> t -> t

  let empty = SSet.empty, TSet.empty

  let is_empty (s,t) = SSet.is_empty s && TSet.is_empty t

  let add_size: _ m = SSet.add <|> map_fst
  let add_type: _ m = TSet.add <|> map_snd
  let add_size': _ m = SSet.add' <|> map_fst
  let add_type': _ m = TSet.add' <|> map_snd

  let union (s1,t1) (s2,t2) =
    Size.Set.union s1 s2,
    Type.Set.union t1 t2
(*
  let union' (s1,t1) (s2,t2) =
    Size.Set.union s1 s2,
    Type.Set.union t1 t2
*)
  let inter (s1,t1) (s2,t2) =
    Size.Set.inter s1 s2,
    Type.Set.inter t1 t2

  let diff (s1,t1) (s2,t2) =
    Size.Set.diff s1 s2,
    Type.Set.diff t1 t2

  let equal (s1,t1) (s2,t2) =
    true
    && Size.Set.equal s1 s2
    && Type.Set.equal t1 t2
end


module Ins = struct
  type t = Size.sub * Type.sub
  type 'a m = 'a -> t -> t

  let empty = SMap.empty, TMap.empty
  let domain (s,t) = Size.Map.keys s, Type.Map.keys t

  let is_empty (s,t) = SMap.is_empty s && TMap.is_empty t

  let add_size v: _ m = SMap.add v <|> map_fst
  let add_type v: _ m = TMap.add v <|> map_snd
  let add_size' v: _ m = SMap.add' v <|> map_fst
  let add_type' v: _ m = TMap.add' v <|> map_snd

  let union merge_s merge_t (s1,t1) (s2,t2) =
    Size.Map.union merge_s s1 s2,
    Type.Map.union merge_t t1 t2

  let union' (s1,t1) (s2,t2) =
    Size.Map.union' s1 s2,
    Type.Map.union' t1 t2

  let equal (s1,t1) (s2,t2) =
    true
    && Size.Map.equal Size.equal s1 s2
    && Type.Map.equal Type.equal t1 t2
end


type gen = Gen.t
type ins = Ins.t





module Subst = struct
  open Types

  let eta subst = SMap.fold Size.compose (fst subst)

  let rec tau subst =
    let refine = function
      | R_top -> R_top
      | R_siz s -> R_siz (eta subst s)
      | R_idx s -> R_idx (eta subst s)
    in
    let ctor : type a. a t_ctor -> a t_ctor = function
      | T_int r -> T_int (refine r)
      | c -> c
    in
    function
    | T_var v as t -> TMap.find ~default:t v (snd subst)
    | T_val (c,a)  -> T_val (ctor c, NSeq.map (tau subst) a)

  let ins subst = Size.Map.map (eta subst) -|- Type.Map.map (tau subst)
end

let failure loc pos fmt = p_error loc "Internal failure (%s)" pos; Format.kasprintf failwith fmt
let unsupported loc pos = failure loc pos "Unsupported"

type align = Left | Center | Right
let align ?(w=Left) ?(n=60) ?(fill=" ") fmter =
  let fill = if fill = "" then " " else fill in
  let open Format in
  kasprintf (fun s ->
      let l = String.length fill in
      let l = ((^) fill)#*(n/l+1) "" in
      let k = String.length s in
      let p = max 0 (n-k) in
      let lp,rp = match w with
        | Left   -> 0, p
        | Center -> p/2, (p+1)/2
        | Right  -> p, 0
      in
      String.sub l 0 lp
      ^ s ^ String.sub l (min n (k+lp)) rp
    ) fmter

let center ?n = align ~w:Center ?n

let debug_level () = 0
let debug ?ends level =
  let open Format in
  (if 0 <= level && level <= debug_level ()
   then (*let () = (fun () -> printf " ")#*level () in*)
(*        let () = printf "| " in*)
        kfprintf
          (fun p -> match ends with
                    | None -> fprintf p "@."
                    | Some s -> fprintf p "%s" s)
   else ifprintf)
    std_formatter


